package org.horizen.habitationlevelpwsssurvey.Main.Home

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.select_scheme_dialog.*
import org.horizen.habitationlevelpwsssurvey.Models.HwData
import org.horizen.habitationlevelpwsssurvey.R
import org.horizen.habitationlevelpwsssurvey.Root.RootDialogFragment
import java.util.ArrayList

class SelectSchemSelect: RootDialogFragment() {
    private val hwDataList = ArrayList<HwData>()
    private val schemeNameList = ArrayList<String>()
    private val schemeCodeList = ArrayList<String>()
    private val schemeSmCodeList = ArrayList<String>()

    private var selectedSchemeCode = ""
    private var selectedSmCode = ""

    companion object {

        fun newInstance() = SelectSchemSelect().apply {

        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.select_scheme_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        scheme.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                selectedSchemeCode = schemeCodeList[p2]
                selectedSmCode = schemeSmCodeList[p2]
            }
        }

        rootVm.hwDataList?.observe(this, Observer { hwData ->
            if (hwData != null && hwData.isNotEmpty()) {
                hwDataList.addAll(hwData)
                getSchemes()
            }
        })

        save.setOnClickListener {
            (activity as HomeActivity).loadKmlTask(selectedSmCode, selectedSchemeCode)
            dialog.dismiss()
        }
    }

    private fun getSchemes() {
        schemeNameList.clear()
        schemeCodeList.clear()
        for (data in hwDataList) {
            if (!schemeSmCodeList.contains(data.scheme_smcode!!)) {
                schemeNameList.add(data.scheme_name!!)
                schemeCodeList.add(data.scheme_code!!)
                schemeSmCodeList.add(data.scheme_smcode!!)
            }
        }

        val scAd = ArrayAdapter<String>(activity!!, R.layout.single_row_list_item, schemeNameList)
        scAd.setDropDownViewResource(R.layout.single_row_list_item)
        scheme.adapter = scAd
        scheme.setSelection(0)
    }

}