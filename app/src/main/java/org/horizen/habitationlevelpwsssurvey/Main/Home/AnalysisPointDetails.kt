package org.horizen.habitationlevelpwsssurvey.Main.Home

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.kbeanie.multipicker.api.CameraImagePicker
import com.kbeanie.multipicker.api.Picker
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback
import com.kbeanie.multipicker.api.entity.ChosenImage
import kotlinx.android.synthetic.main.dialog_analysis_details.*
import org.horizen.habitationlevelpwsssurvey.Helper.GlideApp
import org.horizen.habitationlevelpwsssurvey.Helper.Util
import org.horizen.habitationlevelpwsssurvey.Models.AnalysisData
import org.horizen.habitationlevelpwsssurvey.R
import org.horizen.habitationlevelpwsssurvey.Root.RootDialogFragment
import org.jetbrains.anko.doAsync
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import android.arch.lifecycle.Observer

class AnalysisPointDetails: RootDialogFragment(), ImagePickerCallback {
    private val REQUEST_VIDEO_CAPTURE = 1887

    private var pickerPath: String? = null

    private var point: AnalysisData? = null

    companion object {
        @JvmStatic fun newInstance(point: AnalysisData) = AnalysisPointDetails().apply {
            val b = Bundle()
            b.putSerializable("point", point)
            arguments = b
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dialog_analysis_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        point = arguments?.getSerializable("point") as AnalysisData

        ques.text = "Question: ${point?.question}"
        type.text = "Type: ${point?.type}"

        captureImage.setOnClickListener {
            captureImage()
        }

        captureVideo.setOnClickListener {
            captureVideo()
        }

        save.setOnClickListener {
            rootVm.userDataList?.observe(this, Observer { users ->
                if (users != null) {
                    for (user in users) {
                        point?.remarks = remark.text.toString()
                        point?.device_date = SimpleDateFormat("yyyy-MM-dd").format(Date())
                        point?.device_time = SimpleDateFormat("HH:mm:ss").format(Date())
                        point?.user_id = user.user_id
                        point?.uploaded = false
                        doAsync {
                            localDb.analysisDataDao().updateAnalysisData(point!!)
                        }

                        dialog.dismiss()
                    }
                }
            })
        }
    }

    private var cameraPicker: CameraImagePicker? = null

    private fun captureImage() {
        cameraPicker = CameraImagePicker(this)
        cameraPicker!!.setDebugglable(true)
        cameraPicker!!.setImagePickerCallback(this)
        pickerPath = cameraPicker!!.pickImage()
    }

    private fun captureVideo() {
        val takeVideoIntent = Intent(MediaStore.ACTION_VIDEO_CAPTURE)
        takeVideoIntent.putExtra(MediaStore.EXTRA_OUTPUT, Environment.getExternalStorageDirectory().path + "${UUID.randomUUID()}.mp4")
        startActivityForResult(takeVideoIntent, REQUEST_VIDEO_CAPTURE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == Picker.PICK_IMAGE_CAMERA) {
                if (cameraPicker == null) {
                    cameraPicker = CameraImagePicker(this)
                    cameraPicker!!.setImagePickerCallback(this)
                    cameraPicker!!.reinitialize(pickerPath)
                }
                cameraPicker!!.submit(data)
            } else if (requestCode == REQUEST_VIDEO_CAPTURE) {
                val videoUri = Uri.parse(data?.dataString)
                val path = Util.getRealPathFromURI(activity!!.applicationContext, videoUri)
                Log.e("Video Path", path)
                previewVideo.visibility = View.VISIBLE
                previewVideo.setVideoPath(path)
                previewVideo.setMediaController(null)
                previewVideo.setOnPreparedListener { player ->
                    player.isLooping = true
                }
                previewVideo.start()
            }
        }
    }

    override fun onImagesChosen(list: MutableList<ChosenImage>?) {
        Log.e("Image Picker", "Choosen")
        for (i in list!!.indices) {
            val m = list[i]
            val p = m.originalPath
            val currPath = getOutputMediaFileUri()
            Util.resizeImage(p, currPath.absolutePath)
            val currImagePath = currPath.absolutePath
            previewImage.visibility = View.VISIBLE
            GlideApp.with(activity!!).load(currImagePath).diskCacheStrategy(DiskCacheStrategy.ALL).into(previewImage)
            point?.image_path = currImagePath
        }
    }

    private fun getOutputMediaFileUri(): File {
        var uploadDir = File(Environment.getExternalStorageDirectory().absolutePath + "/CPP")
        if (!uploadDir.exists()) {
            uploadDir.mkdir()
        }
        uploadDir = File(uploadDir, "images")
        if (!uploadDir.exists()) {
            uploadDir.mkdir()
        }
        return File(uploadDir, System.currentTimeMillis().toString() + ".jpg")
    }

    override fun onError(p0: String?) {

    }
}