package org.horizen.habitationlevelpwsssurvey.Main.Home

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.util.Log.e
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.google.android.gms.maps.model.LatLng
import kotlinx.android.synthetic.main.standpost_info_dialog.*
import org.horizen.habitationlevelpwsssurvey.Helper.GlideApp
import org.horizen.habitationlevelpwsssurvey.Models.HabData
import org.horizen.habitationlevelpwsssurvey.Models.PointData
import org.horizen.habitationlevelpwsssurvey.R
import org.horizen.habitationlevelpwsssurvey.Root.RootApplication
import org.horizen.habitationlevelpwsssurvey.Root.RootDialogFragment
import org.jetbrains.anko.doAsync
import java.io.File

class StandPostInfoDialog: RootDialogFragment() {

    private val habArrayList = ArrayList<HabData>()

    private val panchayatCodeList = ArrayList<String>()
    private val panchayatNameList = ArrayList<String>()

    private val mouzaCodeList = ArrayList<String>()
    private val mouzaNameList = ArrayList<String>()

    private val habCodeList = ArrayList<String>()
    private val habNameList = ArrayList<String>()

    private var selectedPanchayatCode = ""
    private var selectedMouzaCode = ""
    private var selectedHabCode = ""

    companion object {
        fun newInstance(standPost : PointData) = StandPostInfoDialog().apply {
            val b = Bundle()
            b.putSerializable("data", standPost)
            arguments = b
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.standpost_info_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val standPost = arguments?.getSerializable("data") as PointData

        e("Stand Post Id", standPost.id)

        val working = if (standPost.tube_working.toInt() == 1) {
            "Working"
        } else {
            "Not Working"
        }

        val desStr = "Scheme Code: ${standPost.scheme_code}\nPanchayat: ${standPost.panchayat}\n" +
                "Village: ${standPost.village}\nHabitation: ${standPost.habitation}\nLatitude: ${standPost.latitude}\n" +
                "Longitude: ${standPost.longitude}\nNearest Location: ${standPost.location}\nWorking Status: $working"

        des.text = desStr

        if (!standPost.imagepath.isNullOrEmpty()) {
            val fileName = standPost.imagepath!!.substring(standPost.imagepath!!.lastIndexOf("/") + 1)
            val settlePath = "$mainPath/Images/$fileName"
            if (File(settlePath).exists()) {
                GlideApp.with(this).load(File(settlePath)).into(source_image)
            }
        }

        hour.setText(standPost.supplyHour)
        min.setText(standPost.supplMin)
        sec.setText(standPost.supplySec)

        close.setOnClickListener {
            dialog.dismiss()
        }

        save.setOnClickListener {
            var h = hour.text.toString()
            var m = min.text.toString()
            var s = sec.text.toString()

            if (h.isEmpty()) {
                h = "00"
            } else if (h.toDouble() < 10) {
                h = "0$h"
            }
            if (m.isEmpty()) {
                m = "00"
            } else if (m.toDouble() < 10) {
                m = "0$m"
            }

            if (s.isEmpty()) {
                s = "00"
            } else if (s.toDouble() < 10) {
                s = "0$s"
            }

            val time = "$h:$m:$s"
            standPost.time_duration = time
            standPost.supplyHour = h
            standPost.supplMin = m
            standPost.supplySec = s
            standPost.uploaded = false
            standPost.habId = selectedHabCode

            doAsync {
                RootApplication.database?.pointDataDao()?.updatePointData(standPost)
                activity?.runOnUiThread {
                    (activity as HomeActivity).uploadSupplyPointData()
                }
            }

            dialog.dismiss()
        }

        navigate.setOnClickListener {

        }

        rootVm.habDataList?.observe(this, Observer { habDataList ->
            if (habDataList != null && habDataList.isNotEmpty()) {
                habArrayList.clear()
                for (habData in habDataList) {
                    if (habData.scheme_code == standPost.scheme_code) {
                        if (!habArrayList.contains(habData)) {
                            habArrayList.add(habData)
                        }
                    }
                }

                loadPanchayats()
            }
        })

        panchayat.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                selectedPanchayatCode = panchayatCodeList[p2]
                getMouzas()
            }
        }


        mouza.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                selectedMouzaCode = mouzaCodeList[p2]
                getHabitations()
            }
        }

        habitation.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                selectedHabCode = habCodeList[p2]
            }
        }

        navigate.setOnClickListener { _ ->
            (activity as HomeActivity).showDirection(LatLng(standPost.latitude.toDouble(), standPost.longitude.toDouble()))
            dialog.dismiss()
        }
    }

    private fun loadPanchayats() {
        panchayatCodeList.clear()
        panchayatNameList.clear()
        for (hab in habArrayList) {
            if (!panchayatCodeList.contains(hab.panc_code)) {
                panchayatCodeList.add(hab.panc_code)
                panchayatNameList.add(hab.panc_name)
            }
        }

        val scAd = ArrayAdapter<String>(activity!!, R.layout.single_row_list_item, panchayatNameList)
        scAd.setDropDownViewResource(R.layout.single_row_list_item)
        panchayat.adapter = scAd
        panchayat.setSelection(0)
    }

    private fun getMouzas() {
        mouzaNameList.clear()
        mouzaCodeList.clear()
        for (hab in habArrayList) {
            if (hab.panc_code == selectedPanchayatCode) {
                if (!mouzaCodeList.contains(hab.vill_code)) {
                    mouzaCodeList.add(hab.vill_code)
                    mouzaNameList.add(hab.vill_name)
                }
            }
        }

        val scAd = ArrayAdapter<String>(activity!!, R.layout.single_row_list_item, mouzaNameList)
        scAd.setDropDownViewResource(R.layout.single_row_list_item)
        mouza.adapter = scAd
        mouza.setSelection(0)
    }

    private fun getHabitations() {
        habCodeList.clear()
        habNameList.clear()
        for (hab in habArrayList) {
            if (hab.panc_code == selectedPanchayatCode && hab.vill_code == selectedMouzaCode) {
                if (!habCodeList.contains(hab.hab_code)) {
                    habCodeList.add(hab.hab_code)
                    habNameList.add(hab.hab_name)
                }
            }
        }

        val scAd = ArrayAdapter<String>(activity!!, R.layout.single_row_list_item, habNameList)
        scAd.setDropDownViewResource(R.layout.single_row_list_item)
        habitation.adapter = scAd
        habitation.setSelection(0)
    }
}