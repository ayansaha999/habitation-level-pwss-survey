package org.horizen.habitationlevelpwsssurvey.Main.Home

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.Location
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.util.Log
import android.util.Log.e
import com.directions.route.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import org.horizen.habitationlevelpwsssurvey.R
import org.horizen.habitationlevelpwsssurvey.Root.RootActivity
import com.google.android.gms.maps.SupportMapFragment
import com.google.maps.android.data.kml.KmlLayer
import kotlinx.android.synthetic.main.content_home.*
import org.horizen.habitationlevelpwsssurvey.Helper.Defaults
import org.horizen.habitationlevelpwsssurvey.Main.Auth.AuthActivity
import org.horizen.habitationlevelpwsssurvey.Main.Upload.UploadStatusActivity
import org.horizen.habitationlevelpwsssurvey.Models.KmlData
import org.horizen.habitationlevelpwsssurvey.Root.RootApplication
import org.jetbrains.anko.doAsync
import org.xmlpull.v1.XmlPullParserException
import java.io.ByteArrayInputStream
import java.io.IOException
import java.util.ArrayList
import com.google.android.gms.maps.model.*
import org.horizen.habitationlevelpwsssurvey.Models.AnalysisData


class HomeActivity : RootActivity(), OnMapReadyCallback, RoutingListener {

    private var selectedSmCode = ""
    private var selectedSchemeCode = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment!!.getMapAsync(this)

        myLocation.setOnClickListener {
            getLastLocation()
        }

        zoomIn.setOnClickListener {
            zoomIn()
        }

        zoomOut.setOnClickListener {
            zoomOut()
        }

        habCheck.setOnClickListener {
            val dialog = HabCheckDialog.newInstance()
            dialog.show(supportFragmentManager, "hab_check")
        }

        selectScheme.setOnClickListener {
            val dialog = SelectSchemSelect.newInstance()
            dialog.show(supportFragmentManager, "scheme_check")
        }

        index.setOnClickListener {
            val dialog = IndexDialog.newInstance(selectedSmCode, selectedSchemeCode)
            dialog.show(supportFragmentManager, "scheme_check")
        }

        uploadStatus.setOnClickListener {
            startActivity(Intent(this@HomeActivity, UploadStatusActivity::class.java))
        }

        logout.setOnClickListener {
            prefs.putBoolean(Defaults.LOGGED_IN, false)
            rootVm.authstatus.value = 0
            startActivity(Intent(this@HomeActivity, AuthActivity::class.java))
        }
    }

    override fun onResume() {
        super.onResume()
        startLocationUpdates()
    }

    override fun onPause() {
        super.onPause()
        stopLocationUpdates()
    }

    override fun onMapReady(map: GoogleMap?) {
        mMap = map!!
        mMap.mapType = GoogleMap.MAP_TYPE_HYBRID
        if (checkPermissions()) {
            startLocationService()
        }

        mMap.setOnMarkerClickListener { marker ->
            if (marker != null) {
                val latLng = marker.position
                checkStandPost(latLng)
                checkAnalysisPoint(marker.position)
            }
            return@setOnMarkerClickListener false
        }

        mMap.uiSettings.isMyLocationButtonEnabled = false

        if (ContextCompat.checkSelfPermission(this@HomeActivity, Manifest.permission.WRITE_CALENDAR)
                != PackageManager.PERMISSION_GRANTED) {
            checkPermissions()
        }
        mMap.isMyLocationEnabled = true
    }

    private fun checkAnalysisPoint(marker: LatLng) {
        doAsync {
            for (point in analysisPointList) {
                if (point.latitude == marker.latitude && point.longitude == marker.longitude) {
                    val dialog = AnalysisPointDetails.newInstance(point)
                    dialog.show(supportFragmentManager, "analysis point")
                }
            }
        }
    }

    private fun checkStandPost(latLng: LatLng?) {
        doAsync {
            val standPosts = RootApplication.database?.pointDataDao()?.getAllPointDatasNormal()
            if (standPosts != null && standPosts.isNotEmpty()) {
                for (stand in standPosts) {
                    if (selectedSchemeCode == stand.scheme_code) {
                        val cLatLng = LatLng(stand.latitude.toDouble(), stand.longitude.toDouble())
                        if (cLatLng == latLng) {
                            runOnUiThread {
                                val dialog = StandPostInfoDialog.newInstance(stand)
                                dialog.show(supportFragmentManager, "STAND_POST_INFO")
                            }
                        }
                    }
                }
            }
        }
    }

    private fun zoomIn() {
        mMap.animateCamera(CameraUpdateFactory.zoomIn())
    }

    private fun zoomOut() {
        mMap.animateCamera(CameraUpdateFactory.zoomOut())
    }


    fun loadKmlTask(smCode: String, schemeCode: String) {
        selectedSmCode = smCode
        selectedSchemeCode = schemeCode
        doAsync {
            val kmlDataList = localDb.kmlDataDao().getAllKmlDatasNormal()
            for (kmlData in kmlDataList) {
                if (kmlData.scheme_smcode == smCode) {
                    loadKmls(kmlData)
                }
            }
        }
    }

    private fun loadKmls(data: KmlData) {
        runOnUiThread {
            mMap.clear()
            if (prefs.getBoolean(Defaults.STAND_POST_WORKING)) {
                loadStandPostWorking()
            }
            if (prefs.getBoolean(Defaults.STAND_POST_NOT_WORKING)) {
                loadStandPostNotWorking()
            }
            if (prefs.getBoolean(Defaults.HABITATION)) {
                loadHabitation(data)
            }
            if (prefs.getBoolean(Defaults.PWSS)) {
                loadScheme(data)
            }
            if (prefs.getBoolean(Defaults.ROAD)) {
                loadRoad(data)
            }
            if (prefs.getBoolean(Defaults.RISING_MAIN)) {
                loadRisingMain(data)
            }
            if (prefs.getBoolean(Defaults.DISTRIBUTION_MAIN_UNDAMAGED)) {
                loadDistributionMain(data)
            }
            if (prefs.getBoolean(Defaults.DISTRIBUTION_MAIN_DAMAGED)) {
                loadDistributionMainDamaged(data)
            }
            if (prefs.getBoolean(Defaults.MOUZA)) {
                loadMouza(data)
            }
            loadAnalysisPoints(data.scheme_smcode)
            locateScheme(data)
        }
    }

    private fun loadAnalysisPoints(scheme_smcode: String) {
        doAsync {
            e("analysisPointList Check", "${analysisPointList.size}")

            runOnUiThread {
                for (point in analysisPointList) {
                    if (point.scheme_smcode == scheme_smcode) {
                        if (point.type.equals("Q")) {
                            val m = MarkerOptions().position(LatLng(point.latitude, point.longitude)).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_analytics_marker_q))
                            loadAnalysisMarker(m)
                        } else {
                            val m = MarkerOptions().position(LatLng(point.latitude, point.longitude)).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_analytics_marker))
                            loadAnalysisMarker(m)
                        }
                    }
                }
            }
        }
    }

    private fun loadAnalysisMarker(m: MarkerOptions) {
        if (!analysisMarkerList.contains(m)) {
            mMap.addMarker(m)
        }
    }

    private fun locateScheme(data: KmlData) {
        if (data.center_lat.isNotEmpty() && data.center_lon.isNotEmpty()) {
            mMap.moveCamera(CameraUpdateFactory.newCameraPosition(CameraPosition(
                    LatLng(data.center_lat.toDouble(), data.center_lon.toDouble()), 10f, 0f, 0f)))
        }
    }

    private fun loadHabitation(data: KmlData) {
        val kmlPath = "$mainPath/${data.scheme_smcode}/habitation.kml"
        Log.e("boundary_path", kmlPath)
        val dirExists = storage.isFileExist(kmlPath)
        if (dirExists) {
            val bytes = storage.readFile(kmlPath)
            try {
                val kmlBoundary = KmlLayer(mMap, ByteArrayInputStream(bytes),
                        applicationContext)
                kmlBoundary.addLayerToMap()
            } catch (e: XmlPullParserException) {
                e.printStackTrace()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

    private fun loadScheme(data: KmlData) {
        val kmlPath = "$mainPath/${data.scheme_smcode}/scheme_boundary.kml"
        Log.e("boundary_path", kmlPath)
        val dirExists = storage.isFileExist(kmlPath)
        if (dirExists) {
            val bytes = storage.readFile(kmlPath)
            try {
                val kmlBoundary = KmlLayer(mMap, ByteArrayInputStream(bytes),
                        applicationContext)
                kmlBoundary.addLayerToMap()
            } catch (e: XmlPullParserException) {
                e.printStackTrace()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

    private fun loadRoad(data: KmlData) {
        val kmlPath = "$mainPath/${data.scheme_smcode}/road.kml"
        Log.e("boundary_path", kmlPath)
        val dirExists = storage.isFileExist(kmlPath)
        if (dirExists) {
            val bytes = storage.readFile(kmlPath)
            try {
                val kmlBoundary = KmlLayer(mMap, ByteArrayInputStream(bytes),
                        applicationContext)
                kmlBoundary.addLayerToMap()
            } catch (e: XmlPullParserException) {
                e.printStackTrace()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

    private fun loadRisingMain(data: KmlData) {
        val kmlPath = "$mainPath/${data.scheme_smcode}/rising_main.kml"
        Log.e("boundary_path", kmlPath)
        val dirExists = storage.isFileExist(kmlPath)
        if (dirExists) {
            val bytes = storage.readFile(kmlPath)
            try {
                val kmlBoundary = KmlLayer(mMap, ByteArrayInputStream(bytes),
                        applicationContext)
                kmlBoundary.addLayerToMap()
            } catch (e: XmlPullParserException) {
                e.printStackTrace()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

    private fun loadDistributionMain(data: KmlData) {
        val kmlPath = "$mainPath/${data.scheme_smcode}/distribution_main.kml"
        Log.e("boundary_path", kmlPath)
        val dirExists = storage.isFileExist(kmlPath)
        if (dirExists) {
            val bytes = storage.readFile(kmlPath)
            try {
                val kmlBoundary = KmlLayer(mMap, ByteArrayInputStream(bytes), applicationContext)
                kmlBoundary.addLayerToMap()
            } catch (e: XmlPullParserException) {
                e.printStackTrace()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }
    private fun loadDistributionMainDamaged(data: KmlData) {
        val kmlPath = "$mainPath/${data.scheme_smcode}/damaged_distribution_main.kml"
        Log.e("boundary_path", kmlPath)
        val dirExists = storage.isFileExist(kmlPath)
        if (dirExists) {
            val bytes = storage.readFile(kmlPath)
            try {
                val kmlBoundary = KmlLayer(mMap, ByteArrayInputStream(bytes), applicationContext)
                kmlBoundary.addLayerToMap()
            } catch (e: XmlPullParserException) {
                e.printStackTrace()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

    private fun loadMouza(data: KmlData) {
        val kmlPath = "$mainPath/${data.scheme_smcode}/mouza.kml"
        Log.e("boundary_path", kmlPath)
        val dirExists = storage.isFileExist(kmlPath)
        if (dirExists) {
            val bytes = storage.readFile(kmlPath)
            try {
                val kmlBoundary = KmlLayer(mMap, ByteArrayInputStream(bytes),
                        applicationContext)
                kmlBoundary.addLayerToMap()
            } catch (e: XmlPullParserException) {
                e.printStackTrace()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

    private fun loadStandPostWorking() {
        doAsync {
            val standPosts = RootApplication.database?.pointDataDao()?.getAllPointDatasNormal()
            if (standPosts != null && standPosts.isNotEmpty()) {
                for (stand in standPosts) {
                    if (selectedSchemeCode == stand.scheme_code) {
                        val latLng = LatLng(stand.latitude.toDouble(), stand.longitude.toDouble())
                        if (stand.tube_working.toInt() == 1) {
                            runOnUiThread {
                                mMap.addMarker(MarkerOptions().title("Stand Post").position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_stand_post)))
                            }
                        }
                    }
                }
            }
        }
    }

    private fun loadStandPostNotWorking() {
        doAsync {
            val standPosts = RootApplication.database?.pointDataDao()?.getAllPointDatasNormal()
            if (standPosts != null && standPosts.isNotEmpty()) {
                for (stand in standPosts) {
                    if (selectedSchemeCode == stand.scheme_code) {
                        val latLng = LatLng(stand.latitude.toDouble(), stand.longitude.toDouble())
                        if (stand.tube_working.toInt() != 1) {
                            runOnUiThread {
                                mMap.addMarker(MarkerOptions().title("Stand Post").position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_stand_post_not_working)))
                            }
                        }
                    }
                }
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (ContextCompat.checkSelfPermission(this@HomeActivity, Manifest.permission.WRITE_CALENDAR)
                != PackageManager.PERMISSION_GRANTED) {
            checkPermissions()
        }
        mMap.isMyLocationEnabled = true
    }

    var startPosition: LatLng? = null
    var endPosition: LatLng? = null

    @SuppressLint("MissingPermission")
    fun showDirection(end: LatLng) {
        fusedLocationClient.lastLocation.addOnSuccessListener { location: Location? ->
            if (location != null) {
                val start = LatLng(location.latitude, location.longitude)
                val cameraUpdate = CameraUpdateFactory.newCameraPosition(CameraPosition.fromLatLngZoom(start, 20f))
                mMap.animateCamera(cameraUpdate)
                startPosition = start
                endPosition = end

                val routing = Routing.Builder()
                        .travelMode(AbstractRouting.TravelMode.DRIVING)
                        .withListener(this)
                        .waypoints(start, end)
                        .key("AIzaSyA1Qv9WcH74N21qKY6ijal5BdWDRZhH8TA")
                        .build()
                routing.execute()
            }
        }
    }

    override fun onRoutingCancelled() {

    }

    override fun onRoutingStart() {

    }

    override fun onRoutingFailure(exception: RouteException?) {

    }

    val polylines = ArrayList<Polyline>()
    private var startMarker: Marker? = null

    override fun onRoutingSuccess(routes: ArrayList<Route>?, position: Int) {
        if (routes != null && routes.isNotEmpty()) {
            val route = routes[position]

            if (polylines.size > 0) {
                for (poly in polylines) {
                    poly.remove()
                }
            }

            val polyOptions = PolylineOptions()
            polyOptions.color(Color.GREEN)
            polyOptions.width((10).toFloat())
            polyOptions.addAll(route.points)
            val polyline = mMap.addPolyline(polyOptions)
            polylines.add(polyline)

            if (startPosition != null) {
                val options = MarkerOptions()
                options.position(startPosition!!)
                options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
                startMarker = mMap.addMarker(options)
            }
        }
    }
}
