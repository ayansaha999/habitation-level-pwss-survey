package org.horizen.habitationlevelpwsssurvey.Main.Auth

import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.text.InputType
import android.view.View
import android.view.WindowManager
import kotlinx.android.synthetic.main.content_auth.*

import org.horizen.habitationlevelpwsssurvey.Helper.Defaults
import org.horizen.habitationlevelpwsssurvey.Main.Home.HomeActivity
import org.horizen.habitationlevelpwsssurvey.R
import org.horizen.habitationlevelpwsssurvey.Root.RootActivity
import org.jetbrains.anko.imageResource
import org.jetbrains.anko.toast

class AuthActivity : RootActivity() {

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_auth)

        if(prefs.getBoolean(Defaults.LOGGED_IN)) {
            startActivity(Intent(this@AuthActivity, HomeActivity::class.java))
            finish()
            return
        }

        prefs.putBoolean(Defaults.STAND_POST_WORKING, true)
        prefs.putBoolean(Defaults.PWSS, true)
        prefs.putBoolean(Defaults.MOUZA, true)
        prefs.putBoolean(Defaults.DISTRIBUTION_MAIN_UNDAMAGED, true)
        prefs.putBoolean(Defaults.DISTRIBUTION_MAIN_DAMAGED, true)
        prefs.putBoolean(Defaults.RISING_MAIN, true)
        prefs.putBoolean(Defaults.HABITATION, true)
        prefs.putBoolean(Defaults.ANALYSIS_POINT_Q, true)
        prefs.putBoolean(Defaults.ANALYSIS_POINT_S, true)

        actionButton.setOnClickListener { _ ->
            when (rootVm.authstatus.value) {
                0 -> {
                    val password = password.text.toString()
                    val username = username.text.toString()

                    if (username.isEmpty()) {
                        toast("Enter username!")
                        return@setOnClickListener
                    }

                    if (password.isEmpty()) {
                        toast("Enter username!")
                        return@setOnClickListener
                    }

                    rootVm.authstatus.value = 1
                    login(username, password)
                }
                2 -> {
                    checkPermissions()
                }
                3 -> {
                    rootVm.authstatus.value = 4
                    downloadHwData(rootVm.activeUser.value!!)
                }
            }
        }

        rootVm.authstatus.observe(this, Observer { status ->
            when (status) {
                0 -> {
                    actionText.text = "Login"
                    actionText.visibility = View.VISIBLE
                    actionProgress.visibility = View.GONE
                    loginLay.visibility = View.VISIBLE
                    actionDetails.visibility = View.GONE
                }
                1 -> {
                    actionText.visibility = View.GONE
                    actionProgress.visibility = View.VISIBLE
                    loginLay.visibility = View.VISIBLE
                    actionDetails.visibility = View.GONE
                }
                2 -> {
                    if(checkPermissions()) {
                        rootVm.authstatus.value = 3
                    }
                    actionText.text = "Agree"
                    actionText.visibility = View.VISIBLE
                    actionProgress.visibility = View.GONE
                    loginLay.visibility = View.GONE
                    actionDetails.visibility = View.VISIBLE
                    rootVm.actionMessage.value = "This app needs permission to store data and media files in your device, get location updates, record audio and access your device camera."
                }
                3 -> {
                    actionText.text = "Download"
                    actionText.visibility = View.VISIBLE
                    actionProgress.visibility = View.GONE
                    loginLay.visibility = View.GONE
                    actionDetails.visibility = View.VISIBLE
                    rootVm.actionMessage.value = "Your assigned location details, form data, kml files and point data for map will be downloaded. Click the download button to download all data."
                }
                4 -> {
                    actionText.visibility = View.GONE
                    actionProgress.visibility = View.VISIBLE
                    loginLay.visibility = View.GONE
                    actionDetails.visibility = View.VISIBLE
                }
            }
        })

        rootVm.actionMessage.observe(this, Observer { message ->
            actionDetails.text = message
        })


        togglePassVisibility.setOnClickListener {
            rootVm.passwordVisible.value = !rootVm.passwordVisible.value!!
        }

        rootVm.passwordVisible.observe(this, Observer { value ->
            if (value != null && value) {
                password.inputType = InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
                password.setSelection(password.length())
                togglePassVisibility.imageResource = R.drawable.design_ic_visibility_off
            } else {
                password.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
                password.setSelection(password.length())
                togglePassVisibility.imageResource = R.drawable.design_ic_visibility

            }
        })
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_PERMISSION -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    rootVm.authstatus.value = 3
                } else {
                   toast("Permission Denied!")
                }
                return
            }

            else -> {
            }
        }
    }
}
