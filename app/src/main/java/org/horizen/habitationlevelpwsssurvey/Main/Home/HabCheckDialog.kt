package org.horizen.habitationlevelpwsssurvey.Main.Home

import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import kotlinx.android.synthetic.main.habitation_modification_dialog.*
import org.horizen.habitationlevelpwsssurvey.Models.HabData
import org.horizen.habitationlevelpwsssurvey.Models.HwData
import org.horizen.habitationlevelpwsssurvey.R
import org.horizen.habitationlevelpwsssurvey.Root.RootApplication
import org.horizen.habitationlevelpwsssurvey.Root.RootDialogFragment
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.support.v4.toast
import java.util.*

class HabCheckDialog: RootDialogFragment() {

    companion object {

        fun newInstance() = HabCheckDialog().apply {

        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.habitation_modification_dialog, container, false)
    }

    private val hwDataList = ArrayList<HwData>()
    private val habDataList = ArrayList<HabData>()

    private val schemeCodeList = ArrayList<String>()
    private val schemeNameList = ArrayList<String>()

    private val panchayatNameList = ArrayList<String>()
    private val panchayatCodeList = ArrayList<String>()
    private val mouzaNameList = ArrayList<String>()
    private val mouzaCodeList = ArrayList<String>()


    private val smCodeList = ArrayList<String>()

    private var newHabNameVisible = false

    private lateinit var selectedSchemeCode: String
    private lateinit var selectedPanchayatCode: String
    private lateinit var selectedMouzaCode: String
    private lateinit var selectedSchemeSmCOde: String
    private lateinit var selectedPanchayatName: String
    private lateinit var selectedMouzaName: String

    private var visibleHabData: HabData? = null


    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog.setCanceledOnTouchOutside(false)
        dialog.setCancelable(false)

        rootVm.hwDataList?.observe(this, Observer { hwData ->
            if (hwData != null && hwData.isNotEmpty()) {
                hwDataList.addAll(hwData)
                getSchemes()
            }
        })

        rootVm.habFilterOpen.observe(this, Observer { filterVisibile ->
            if (filterVisibile == null || filterVisibile) {
                filter.text = "Close Filter"
                filterLay.visibility = View.VISIBLE
            } else {
                filter.text = "Open Filter"
                filterLay.visibility = View.GONE
            }
        })

        filter.setOnClickListener {
            rootVm.habFilterOpen.value = !rootVm.habFilterOpen.value!!
        }

        scheme.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                selectedSchemeCode = schemeCodeList[p2]
                selectedSchemeSmCOde = smCodeList[p2]
                getPanchayats()
            }
        }

        gp.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                selectedPanchayatCode = panchayatCodeList[p2]
                selectedPanchayatName = panchayatNameList[p2]
                getMouzas()
            }
        }


        mouza.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                selectedMouzaCode = mouzaCodeList[p2]
                selectedMouzaName = mouzaNameList[p2]
                getHabitations()
            }
        }

        addNewHab.setOnClickListener {
            newHabNameVisible = !newHabNameVisible
            if (newHabNameVisible) {
                newHabName.visibility = View.VISIBLE
                addNewHab.text = "Done"

            } else {
                val newHaNameStr = newHabName.text.toString()
                addNewHab.text = "Add New Habitation"
                rootVm.userDataList?.observe(this, Observer { users ->
                    if (users != null) {
                        for (user in users) {
                            if (user.status) {
                                doAsync {
                                    val hab = HabData()
                                    hab.id = UUID.randomUUID().toString()
                                    hab.scheme_smcode = selectedSchemeSmCOde
                                    hab.scheme_code = selectedSchemeCode
                                    hab.user_id = user.user_id
                                    hab.block_id = user.block_id
                                    hab.dist_id = user.dist_id
                                    hab.supply_level = "0"
                                    hab.hab_name = newHaNameStr
                                    hab.new_habname = newHaNameStr
                                    hab.hab_correction = "1"
                                    hab.panc_code = selectedPanchayatCode
                                    hab.vill_code = selectedMouzaCode
                                    hab.panc_name = selectedPanchayatName
                                    hab.vill_name = selectedMouzaName
                                    hab.uploaded = false
                                    hab.device_date = rootVm.getTodayDate()
                                    hab.device_time = rootVm.getTodayTime()
                                    visibleHabData = hab
                                    RootApplication.database?.habDataDao()?.insertHabData(hab)
                                }
                            }
                        }
                    }
                })


                newHabName.visibility = View.GONE
            }
        }

        save.setOnClickListener {
            (activity as HomeActivity).uploadHabData()
            dialog.dismiss()
        }
    }

    private fun getPanchayats() {
        panchayatNameList.clear()
        panchayatCodeList.clear()
        rootVm.habDataList?.observe(this, Observer { habData ->
            if (habData != null && habData.isNotEmpty()) {
                for (hab in habData) {
                    if (selectedSchemeSmCOde == hab.scheme_smcode) {
                        habDataList.add(hab)
                        if (!panchayatCodeList.contains(hab.panc_code)) {
                            panchayatCodeList.add(hab.panc_code)
                            panchayatNameList.add(hab.panc_name)
                        }
                    }
                }

                val scAd = ArrayAdapter<String>(activity!!, R.layout.single_row_list_item, panchayatNameList)
                scAd.setDropDownViewResource(R.layout.single_row_list_item)
                gp.adapter = scAd
                gp.setSelection(0)
            }
        })
    }

    private fun getMouzas() {
        mouzaNameList.clear()
        mouzaCodeList.clear()
        for (hab in habDataList) {
            if (selectedSchemeSmCOde == hab.scheme_smcode && hab.panc_code == selectedPanchayatCode) {
                if (!mouzaCodeList.contains(hab.vill_code)) {
                    mouzaCodeList.add(hab.vill_code)
                    mouzaNameList.add(hab.vill_name)
                }
            }
        }

        val scAd = ArrayAdapter<String>(activity!!, R.layout.single_row_list_item, mouzaNameList)
        scAd.setDropDownViewResource(R.layout.single_row_list_item)
        mouza.adapter = scAd
        mouza.setSelection(0)
    }

    private fun getHabitations() {
        rootVm.habDataList?.observe(this, Observer { habData ->
            if (habData != null && habData.isNotEmpty()) {
                val newHabDataList = ArrayList<HabData>()
                for (hab in habData) {
                    if (selectedSchemeSmCOde == hab.scheme_smcode && hab.panc_code == selectedPanchayatCode
                            && hab.vill_code == selectedMouzaCode) {
                        if (!newHabDataList.contains(hab)) {
                            newHabDataList.add(hab)
                        }
                    }
                }

                val adapter = HabAdapter(newHabDataList)
                habList.adapter = adapter
                for (index in 0 until newHabDataList.size) {
                    if (visibleHabData == newHabDataList[index]) {
                        habList.setSelection(index)
                    }
                }
            }
        })
    }

    private fun getSchemes() {
        schemeNameList.clear()
        smCodeList.clear()
        schemeCodeList.clear()
        for (data in hwDataList) {
            if (!schemeNameList.contains(data.scheme_name!!)) {
                schemeNameList.add(data.scheme_name!!)
                smCodeList.add(data.scheme_smcode!!)
                schemeCodeList.add(data.scheme_code!!)
            }
        }

        val scAd = ArrayAdapter<String>(activity!!, R.layout.single_row_list_item, schemeNameList)
        scAd.setDropDownViewResource(R.layout.single_row_list_item)
        scheme.adapter = scAd
        scheme.setSelection(0)
    }

    inner class HabAdapter(private val habs : ArrayList<HabData>) : BaseAdapter() {
        private val mInflater: LayoutInflater = LayoutInflater.from(activity)

        @SuppressLint("ViewHolder")
        override fun getView(position: Int, p1: View?, parent: ViewGroup?): View {
            val view = mInflater.inflate(R.layout.single_hab_list_item, parent, false)

            val hab = habs[position]
            val name = view.findViewById<TextView>(R.id.habName)
            val updatedName = view.findViewById<EditText>(R.id.habUpdatedName)
            val update = view.findViewById<ImageView>(R.id.update)

            val updateRemark = view.findViewById<ImageView>(R.id.updateRemark)
            val habRemark = view.findViewById<EditText>(R.id.habRemark)

            val delete = view.findViewById<ImageView>(R.id.delete)
            val root = view.findViewById<LinearLayout>(R.id.rootBack)

            name.text = hab.hab_name

            val selectCon = view.findViewById<Spinner>(R.id.selectCon)
            val arrayList = getSupplyConditionList()
            val adapter = ArrayAdapter<String>(activity!!, R.layout.single_row_list_item, arrayList)
            selectCon.adapter = adapter
            selectCon.setSelection(0)

            for (index in 0 until arrayList.size) {
                if (arrayList[index] == hab.supply_remarks) {
                    selectCon.setSelection(index)
                }
            }

            updatedName.setText(hab.new_habname)

            delete.setOnClickListener {
                doAsync {
                    hab.hab_correction = "3"
                    hab.uploaded = false
                    hab.device_date = rootVm.getTodayDate()
                    hab.device_time = rootVm.getTodayTime()
                    visibleHabData = hab
                    RootApplication.database?.habDataDao()?.updateHabData(hab)
                }
            }

            selectCon.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(p0: AdapterView<*>?) {

                }

                override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                    var selected = ""
                    var available = "Yes"
                    if (p2 > 0) {
                        selected = arrayList[p2]
                    }
                    available = if (p2 > 4) {
                        "No"
                    } else {
                        "Yes"
                    }
                    if (selected != hab.supply_remarks) {
                        doAsync {
                            hab.supply_remarks = selected
                            hab.supply_status = available
                            hab.uploaded = false
                            hab.device_date = rootVm.getTodayDate()
                            hab.device_time = rootVm.getTodayTime()
                            if (hab.hab_correction.isEmpty()) {
                                hab.hab_correction = "0"
                            }
                            visibleHabData = hab
                            RootApplication.database?.habDataDao()?.updateHabData(hab)
                        }
                    }
                }
            }

            when (hab.hab_correction) {
                "1" -> {
                    root.setBackgroundResource(R.color.colorCyan)
                }
                "2" -> {
                    root.setBackgroundResource(R.color.colorYellow)
                }
                "3" -> {
                    root.setBackgroundResource(R.color.colorRed)
                }
            }

            update.setOnClickListener {
                val newName = updatedName.text.toString()
                if (newName.isNotEmpty()) {
                    update.visibility = View.GONE
                    doAsync {
                        hab.new_habname = newName
                        hab.hab_correction = "2"
                        hab.uploaded = false
                        hab.device_date = rootVm.getTodayDate()
                        hab.device_time = rootVm.getTodayTime()
                        visibleHabData = hab
                        RootApplication.database?.habDataDao()?.updateHabData(hab)
                    }
                } else {
                    toast("Enter name to update!")
                }
            }

            updateRemark.setOnClickListener {
                val newRemark = updatedName.text.toString()
                if (newRemark.isNotEmpty()) {
                    updateRemark.visibility = View.GONE
                    doAsync {
                        hab.hab_remarks = newRemark
                        hab.uploaded = false
                        hab.device_date = rootVm.getTodayDate()
                        hab.device_time = rootVm.getTodayTime()
                        visibleHabData = hab
                        RootApplication.database?.habDataDao()?.updateHabData(hab)
                    }
                } else {
                    toast("Enter habitation remarks!")
                }
            }

            updatedName.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(p0: Editable?) {
                    val newName = updatedName.text.toString()
                    if (newName.isNotEmpty()) {
                        update.visibility = View.VISIBLE
                    } else {
                        update.visibility = View.GONE
                    }
                }

                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                }

                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    val newName = updatedName.text.toString()
                    if (newName.isNotEmpty()) {
                        update.visibility = View.VISIBLE
                    } else {
                        update.visibility = View.GONE
                    }
                }

            })

            habRemark.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(p0: Editable?) {
                    val newRemark = habRemark.text.toString()
                    if (newRemark.isNotEmpty()) {
                        updateRemark.visibility = View.VISIBLE
                    } else {
                        updateRemark.visibility = View.GONE
                    }
                }

                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                }

                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    val newRemark = habRemark.text.toString()
                    if (newRemark.isNotEmpty()) {
                        updateRemark.visibility = View.VISIBLE
                    } else {
                        updateRemark.visibility = View.GONE
                    }
                }

            })
            return view
        }

        override fun getItem(p0: Int): Any {
            return habs[p0]
        }

        override fun getItemId(p0: Int): Long {
            return p0.toLong()
        }

        override fun getCount(): Int {
            return habs.size
        }
    }

    private fun getSupplyConditionList(): ArrayList<String> {
        val resultArrayList = ArrayList<String>()
        resultArrayList.add("Select supply status")
        resultArrayList.add("Pressure not adequate")
        resultArrayList.add("Insufficient water supply")
        resultArrayList.add("Sufficient water supply")
        resultArrayList.add("No pipe line")
        resultArrayList.add("Pipe line not fully Covered")
        resultArrayList.add("Pipe line exist but no water supply")
        resultArrayList.add("Stand Post insufficient")
        resultArrayList.add("Defunct Tubewell")
        resultArrayList.add("Defunct Stand Post")
        resultArrayList.add("Damage Pipe line ")
        resultArrayList.add("Defunct Pump")
        resultArrayList.add("Defunct Motor")
        resultArrayList.add("No Electricity")
        return resultArrayList
    }
}