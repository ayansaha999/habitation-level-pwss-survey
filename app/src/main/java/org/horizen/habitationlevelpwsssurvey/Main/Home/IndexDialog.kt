package org.horizen.habitationlevelpwsssurvey.Main.Home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.index_dialog.*
import org.horizen.habitationlevelpwsssurvey.Helper.Defaults
import org.horizen.habitationlevelpwsssurvey.R
import org.horizen.habitationlevelpwsssurvey.Root.RootDialogFragment
import org.jetbrains.anko.sdk25.coroutines.onCheckedChange

class IndexDialog: RootDialogFragment() {

    companion object {

        fun newInstance(smCode: String, schemeCode: String) = IndexDialog().apply {
            val b = Bundle()
            b.putString("smCode", smCode)
            b.putString("schemeCode", schemeCode)

            arguments = b
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.index_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val smCode = arguments?.getString("smCode") as String
        val schemeCode = arguments?.getString("schemeCode") as String

        stand_post_working.isChecked = prefs.getBoolean(Defaults.STAND_POST_WORKING)
        stand_post_not_working.isChecked = prefs.getBoolean(Defaults.STAND_POST_NOT_WORKING)

        road.isChecked = prefs.getBoolean(Defaults.ROAD)
        scheme.isChecked = prefs.getBoolean(Defaults.PWSS)
        mouza.isChecked = prefs.getBoolean(Defaults.MOUZA)
        habitation.isChecked = prefs.getBoolean(Defaults.HABITATION)

        distribution_undamaged.isChecked = prefs.getBoolean(Defaults.DISTRIBUTION_MAIN_UNDAMAGED)
        distribution_damaged.isChecked = prefs.getBoolean(Defaults.DISTRIBUTION_MAIN_DAMAGED)

        rising.isChecked = prefs.getBoolean(Defaults.RISING_MAIN)

        analysis_point_s.isChecked = prefs.getBoolean(Defaults.ANALYSIS_POINT_S)
        analysis_point_q.isChecked = prefs.getBoolean(Defaults.ANALYSIS_POINT_Q)

        stand_post_working.setOnCheckedChangeListener { _, b ->
            prefs.putBoolean(Defaults.STAND_POST_WORKING, b)
        }

        stand_post_not_working.setOnCheckedChangeListener { _, b ->
            prefs.putBoolean(Defaults.STAND_POST_NOT_WORKING, b)
        }

        road.setOnCheckedChangeListener { _, b ->
            prefs.putBoolean(Defaults.ROAD, b)
        }

        scheme.setOnCheckedChangeListener { _, b ->
            prefs.putBoolean(Defaults.PWSS, b)
        }

        mouza.setOnCheckedChangeListener { _, b ->
            prefs.putBoolean(Defaults.MOUZA, b)
        }

        habitation.setOnCheckedChangeListener { _, b ->
            prefs.putBoolean(Defaults.HABITATION, b)
        }

        distribution_undamaged.setOnCheckedChangeListener { _, b ->
            prefs.putBoolean(Defaults.DISTRIBUTION_MAIN_UNDAMAGED, b)
        }

        distribution_damaged.setOnCheckedChangeListener { _, b ->
            prefs.putBoolean(Defaults.DISTRIBUTION_MAIN_DAMAGED, b)
        }

        rising.setOnCheckedChangeListener { _, b ->
            prefs.putBoolean(Defaults.RISING_MAIN, b)
        }

        analysis_point_s.setOnCheckedChangeListener { _, b ->
            prefs.putBoolean(Defaults.ANALYSIS_POINT_S, b)
        }

        analysis_point_q.setOnCheckedChangeListener { _, b ->
            prefs.putBoolean(Defaults.ANALYSIS_POINT_Q, b)
        }

        save.setOnClickListener {
            (activity as HomeActivity).loadKmlTask(smCode, schemeCode)
            dialog.dismiss()
        }
    }
}
