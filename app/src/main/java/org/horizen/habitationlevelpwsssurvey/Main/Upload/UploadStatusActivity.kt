package org.horizen.habitationlevelpwsssurvey.Main.Upload

import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.os.Bundle
import android.os.Environment
import android.util.Log.e
import org.horizen.habitationlevelpwsssurvey.R

import org.horizen.habitationlevelpwsssurvey.Root.RootActivity
import com.anychart.anychart.AnyChart
import kotlinx.android.synthetic.main.content_upload_status.*
import com.anychart.anychart.DataEntry
import com.anychart.anychart.EnumsAlign
import com.anychart.anychart.LegendLayout
import com.anychart.anychart.ValueDataEntry
import android.widget.Toast
import com.anychart.anychart.chart.common.ListenersInterface
import com.anychart.anychart.chart.common.Event
import jxl.Workbook
import jxl.WorkbookSettings
import jxl.format.Alignment
import jxl.format.Colour
import jxl.write.*
import org.horizen.habitationlevelpwsssurvey.Models.HabData
import org.horizen.habitationlevelpwsssurvey.Models.PointData
import org.jetbrains.anko.doAsync
import java.io.File
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList


class UploadStatusActivity : RootActivity() {

    private val currentHabDataList = ArrayList<HabData>()
    private val currentSourceDataList = ArrayList<PointData>()

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_upload_status)

        chart.setProgressBar(findViewById(R.id.progress_bar))
        val pie = AnyChart.pie()

        rootVm.habDataList?.observe(this, Observer { habDataList ->
            val data = ArrayList<DataEntry>()
            currentHabDataList.clear()
            if (habDataList != null && habDataList.isNotEmpty()) {
                currentHabDataList.addAll(habDataList)
                var habDataLeft = 0
                var habDataSynced = 0
                for (habData in currentHabDataList) {
                    if (!habData.uploaded) {
                        habDataLeft += 1
                    } else {
                        habDataSynced += 1
                    }
                }
                habMod.text = "Habitations Not Synced: $habDataLeft"
                data.add(ValueDataEntry("Habitations Not Synced", habDataLeft))
                data.add(ValueDataEntry("Habitations Synced", habDataSynced))

            } else {
                habMod.text = "Habitations Not Synced: 0"
                data.add(ValueDataEntry("Habitations Not Synced", 0))
                data.add(ValueDataEntry("Habitations Synced", 0))
            }

            rootVm.pointDataList?.observe(this, Observer { pointDataList ->
                currentSourceDataList.clear()
                if (pointDataList != null && pointDataList.isNotEmpty()) {
                    currentSourceDataList.addAll(pointDataList)
                    var sourceDataLeft = 0
                    var sourceDataSynced = 0
                    for (habData in currentSourceDataList) {
                        if (!habData.uploaded) {
                            sourceDataLeft += 1
                        } else {
                            sourceDataSynced += 1
                        }
                    }
                    totSupply.text = "Source Not Synced: $sourceDataLeft"
                    data.add(ValueDataEntry("Source Not Synced", sourceDataLeft))
                    data.add(ValueDataEntry("Source Synced", sourceDataSynced))
                } else {
                    totSupply.text = "Source Not Synced: 0"
                    data.add(ValueDataEntry("Source Not Synced", 0))
                    data.add(ValueDataEntry("Source Synced", 0))
                }
                pie.setData(data)
            })
        })

        habSync.setOnClickListener {
            uploadHabData()
        }

        supplySync.setOnClickListener {
            uploadSupplyPointData()
        }
        
        pie.setTitle("Current Status")

        pie.labels.setPosition("outside")

        pie.legend.title.setEnabled(true)
        pie.legend.title
                .setText("Data Type")
                .setPadding(0.0, 0.0, 10.0, 0.0)

        pie.legend
                .setPosition("center-bottom")
                .setItemsLayout(LegendLayout.HORIZONTAL)
                .setAlign(EnumsAlign.CENTER)

        chart.setChart(pie)

        download_report.setOnClickListener { _ ->
            downloadDataExcel(currentHabDataList, currentSourceDataList)
        }
    }

    private fun downloadDataExcel(currentHabDataList: ArrayList<HabData>, currentSourceDataList: ArrayList<PointData>) {
        doAsync {
            val sd = Environment.getExternalStorageDirectory()
            val fTime = System.currentTimeMillis()
            val xlsFile = "survey_data_$fTime.xls"

            val directory = File(sd.absolutePath + "/Habitation Level Pwss Survey/Report/")
            if (!directory.isDirectory) {
                directory.mkdirs()
            }
            e("File Name", xlsFile)
            val file = File(directory, xlsFile)
            val wbSettings = WorkbookSettings()
            wbSettings.locale = Locale("en", "EN")
            val workbook = Workbook.createWorkbook(file, wbSettings)

            if (currentHabDataList.isNotEmpty()) {
                try {
                    val sheet = workbook.createSheet("Habitation Data", 0)

                    val headCellFormat = WritableCellFormat()
                    headCellFormat.setBorder(Border.ALL, BorderLineStyle.THIN)

                    sheet.addCell(Label(0, 0, "#", headCellFormat))
                    sheet.addCell(Label(1, 0, "Habitation Id", headCellFormat))
                    sheet.addCell(Label(2, 0, "Panchayat Code", headCellFormat))
                    sheet.addCell(Label(3, 0, "Panchayat Name", headCellFormat))
                    sheet.addCell(Label(4, 0, "Mouza Code", headCellFormat))
                    sheet.addCell(Label(5, 0, "Mouza Name", headCellFormat))
                    sheet.addCell(Label(6, 0, "Habitation Code", headCellFormat))
                    sheet.addCell(Label(7, 0, "Habitation Name", headCellFormat))
                    sheet.addCell(Label(8, 0, "Scheme Smcode", headCellFormat))
                    sheet.addCell(Label(9, 0, "Scheme Code", headCellFormat))
                    sheet.addCell(Label(10, 0, "Suggestion", headCellFormat))
                    sheet.addCell(Label(11, 0, "New Habitation Name", headCellFormat))
                    sheet.addCell(Label(12, 0, "Supply Status", headCellFormat))
                    sheet.addCell(Label(13, 0, "Supply Remarks", headCellFormat))
                    sheet.addCell(Label(14, 0, "Device Date", headCellFormat))
                    sheet.addCell(Label(15, 0, "Device Time", headCellFormat))
                    sheet.addCell(Label(16, 0, "Sync Status", headCellFormat))

                    for (i in 0 until currentHabDataList.size) {
                        val row = currentHabDataList[i]

                        val cellFormat = WritableCellFormat()
                        cellFormat.setBorder(Border.ALL, BorderLineStyle.THIN)

                        var suggestion ="None"
                        when (row.hab_correction) {
                            "1" -> {
                                suggestion = "Add"
                                cellFormat.setBackground(Colour.LIGHT_GREEN)

                            }
                            "2" -> {
                                suggestion = "Update"
                                cellFormat.setBackground(Colour.VERY_LIGHT_YELLOW)

                            }
                            "3" -> {
                                suggestion = "Delete"
                                cellFormat.setBackground(Colour.RED)
                            }
                            else -> {
                                suggestion = "None"
                            }
                        }

                        val status = if (row.uploaded) {
                            "Synced"
                        } else {
                            "Not Synced"
                        }

                        sheet.addCell(Label(0, i + 1, i.toString(), cellFormat))
                        sheet.addCell(Label(1, i + 1, row.id, cellFormat))
                        sheet.addCell(Label(2, i + 1, row.panc_code, cellFormat))
                        sheet.addCell(Label(3, i + 1, row.panc_name, cellFormat))
                        sheet.addCell(Label(4, i + 1, row.vill_code, cellFormat))
                        sheet.addCell(Label(5, i + 1, row.vill_name, cellFormat))
                        sheet.addCell(Label(6, i + 1, row.hab_code, cellFormat))
                        sheet.addCell(Label(7, i + 1, row.hab_name, cellFormat))
                        sheet.addCell(Label(8, i + 1, row.scheme_smcode, cellFormat))
                        sheet.addCell(Label(9, i + 1, row.scheme_code, cellFormat))
                        sheet.addCell(Label(10, i + 1, suggestion, cellFormat))
                        sheet.addCell(Label(11, i + 1, row.new_habname, cellFormat))
                        sheet.addCell(Label(12, i + 1, row.supply_status, cellFormat))
                        sheet.addCell(Label(13, i + 1, row.supply_remarks, cellFormat))
                        sheet.addCell(Label(14, i + 1, row.device_date, cellFormat))
                        sheet.addCell(Label(15, i + 1, row.device_time, cellFormat))
                        sheet.addCell(Label(16, i + 1, status, cellFormat))

                    }
                    workbook.write()
                    workbook.close()
                } catch (e: IOException) {
                    e.printStackTrace()
                } catch (w: WriteException) {
                    w.printStackTrace()
                }
            }

            if (currentSourceDataList.isNotEmpty()) {
                try {
                    val thumbPath = "http://maps.wbphed.gov.in/spot_source/mobile_service_test/thumb/"

                    val sheet = workbook.createSheet("Source Data", 1)

                    val cellFont = WritableFont(WritableFont.COURIER, 16)
                    cellFont.setBoldStyle(WritableFont.BOLD)

                    val headCellFormat = WritableCellFormat(cellFont)
                    headCellFormat.alignment = Alignment.CENTRE
                    headCellFormat.setBorder(Border.ALL, BorderLineStyle.THIN)

                    sheet.addCell(Label(0, 0, "#", headCellFormat))
                    sheet.addCell(Label(1, 0, "Source Id", headCellFormat))
                    sheet.addCell(Label(2, 0, "Latitude", headCellFormat))
                    sheet.addCell(Label(3, 0, "Longitude", headCellFormat))
                    sheet.addCell(Label(4, 0, "Habitation", headCellFormat))
                    sheet.addCell(Label(5, 0, "Working Status", headCellFormat))
                    sheet.addCell(Label(6, 0, "Village", headCellFormat))
                    sheet.addCell(Label(7, 0, "Scheme Code", headCellFormat))
                    sheet.addCell(Label(8, 0, "Image Link", headCellFormat))
                    sheet.addCell(Label(9, 0, "Sl No", headCellFormat))
                    sheet.addCell(Label(10, 0, "Supply Hour", headCellFormat))
                    sheet.addCell(Label(11, 0, "Supply Minute", headCellFormat))
                    sheet.addCell(Label(12, 0, "Supply Second", headCellFormat))
                    sheet.addCell(Label(13, 0, "Time Duration", headCellFormat))
                    sheet.addCell(Label(14, 0, "Location", headCellFormat))
                    sheet.addCell(Label(15, 0, "Habitation Id", headCellFormat))
                    sheet.addCell(Label(16, 0, "Sync Status", headCellFormat))

                    for (i in 0 until currentSourceDataList.size) {
                        val row = currentSourceDataList[i]

                        val status = if (row.uploaded) {
                            "Synced"
                        } else {
                            "Not Synced"
                        }

                        sheet.addCell(Label(0, i + 1, i.toString()))
                        sheet.addCell(Label(1, i + 1, row.id))
                        sheet.addCell(Label(2, i + 1, row.latitude))
                        sheet.addCell(Label(3, i + 1, row.longitude))
                        sheet.addCell(Label(4, i + 1, row.habitation))
                        sheet.addCell(Label(5, i + 1, row.tube_working))
                        sheet.addCell(Label(6, i + 1, row.village))
                        sheet.addCell(Label(7, i + 1, row.scheme_code))
                        sheet.addCell(Label(8, i + 1, "$thumbPath${row.imagepath}"))
                        sheet.addCell(Label(9, i + 1, row.slno))
                        sheet.addCell(Label(10, i + 1, row.supplyHour))
                        sheet.addCell(Label(11, i + 1, row.supplMin))
                        sheet.addCell(Label(12, i + 1, row.supplySec))
                        sheet.addCell(Label(13, i + 1, row.time_duration))
                        sheet.addCell(Label(14, i + 1, row.location))
                        sheet.addCell(Label(15, i + 1, row.habId))
                        sheet.addCell(Label(16, i + 1, status))
                    }
                    workbook.write()
                    workbook.close()
                } catch (e: IOException) {
                    e.printStackTrace()
                } catch (w: WriteException) {
                    w.printStackTrace()
                }
            }
        }
    }
}
