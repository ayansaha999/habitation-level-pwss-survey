package org.horizen.habitationlevelpwsssurvey.Models

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import java.io.Serializable

@Entity
data class HwData(

        @PrimaryKey
        var id: String,
        var scheme_name: String?,
        var scheme_code: String?,
        var scheme_smcode: String?,
        var scheme_status: String?,
        var hab_name: String?,
        var mouza_name: String?,
        var jlno: String?,
        var lat: String?,
        var lon: String?,
        var tw_no: String?,
        var zone: String?,
        var tw_condition: String?,
        var how_long: String?,
        var nw_reason: String?,
        var pump_hours: String?,
        var supply_hours: String?,
        var litho_collected: String?,
        var yield: String?,
        var qp_odor: String?,
        var qp_color: String?,
        var qp_opacity: String?,
        var chl_functioning: String?,
        var chl_done: String?,
        var chl_frequency: String?,
        var provision_treatment: String?,
        var chl_operatortrain: String?,
        var provision_as: String?,
        var provision_fe: String?,
        var provision_f: String?,
        var tp_working: String?,
        var ohr_exist_stat: String?,
        var ohr_height: String?,
        var ohr_capacity: String?,
        var cwr_exist_status: String?,
        var cwr_height: String?,
        var condition_highlift: String?,
        var supply_level1: String?,
        var supply_level2: String?,
        var supply_level3: String?,
        var supply_level4: String?,
        var water_level: String?,
        var name_keyinformant: String?,
        var mob_keyinformant: String?
): Serializable {
    @Ignore
    constructor(): this("", "","", "", "","", "", "",
            "", "", "", "", "", "", "", "", "",
            "", "", "", "", "", "", "", "",
            "", "", "", "", "", "", "",
            "", "", "", "", "", "", "",
            "", "", "", "", "")
}