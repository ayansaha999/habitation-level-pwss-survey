package org.horizen.habitationlevelpwsssurvey.Models

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import java.io.Serializable

@Entity
data class User(

        @PrimaryKey
        var id: String,

        var user_id: String,

        var dist_id: String,

        var block_id: String,

        var session_key: String,

        var user_priority: String,

        var status: Boolean,

        var version: String
): Serializable {
    @Ignore
    constructor(): this("", "", "", "", "", "", false, "")
}