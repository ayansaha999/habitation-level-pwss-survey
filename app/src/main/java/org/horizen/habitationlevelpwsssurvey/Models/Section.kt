package org.horizen.habitationlevelpwsssurvey.Models

import android.arch.persistence.room.Ignore
import java.io.Serializable

data class Section(
        var number: Int,
        var name: String,
        var fragment: Int

): Serializable {
    @Ignore
    constructor(): this(0, "", 0)
}