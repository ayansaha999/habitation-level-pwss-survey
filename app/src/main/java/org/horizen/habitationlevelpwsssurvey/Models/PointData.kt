package org.horizen.habitationlevelpwsssurvey.Models

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import java.io.Serializable

@Entity
data class PointData(

        @PrimaryKey
        var id: String,
        var latitude: String,
        var longitude: String,
        var habitation: String,
        var tube_working: String,
        var panchayat: String,
        var village: String,
        var scheme_code: String,
        var imagepath: String?,
        var slno: String,
        var supplyHour: String,
        var supplMin: String,
        var supplySec: String,
        var time_duration: String,
        var uploaded: Boolean,
        var location: String,
        var habId: String
): Serializable {
    @Ignore
    constructor(): this("", "", "", "", "", "", "",
            "", "", "", "", "", "", "", true, "",  "")
}