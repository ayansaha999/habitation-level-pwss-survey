package org.horizen.habitationlevelpwsssurvey.Models

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import java.io.Serializable

@Entity
data class AnalysisData(

        @PrimaryKey
        var sp_id: String,

        var number: String,

        var latitude: Double,

        var longitude: Double,

        var type: String,

        var question: String,

        var scheme_code: String,

        var scheme_smcode: String,

        var image_path: String,

        var remarks: String,

        var device_date: String,

        var device_time: String,

        var user_id: String,

        var uploaded: Boolean

): Serializable {
    constructor(): this("", "", 0.0, 0.0, "", "", "", "", "", "", "", "", "",true)
}