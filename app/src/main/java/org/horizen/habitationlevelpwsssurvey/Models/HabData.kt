package org.horizen.habitationlevelpwsssurvey.Models

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import java.io.Serializable

@Entity
data class HabData(

        @PrimaryKey
        var id: String,
        var dist_id: String,
        var block_id: String,
        var supply_level: String,
        var user_id: String,
        var panc_code: String,
        var panc_name: String,
        var vill_code: String,
        var vill_name: String,
        var hab_code: String,
        var hab_name: String,
        var scheme_smcode: String,
        var scheme_code: String,
        var hab_correction: String,
        var new_habname: String,
        var supply_status: String,
        var supply_remarks: String,
        var hab_remarks: String,
        var uploaded: Boolean,
        var device_date: String,
        var device_time: String
): Serializable {
    @Ignore
    constructor(): this("", "","", "0", "", "", "", "", "", "", "", "", "", "", "","","", "", true, "", "")
}