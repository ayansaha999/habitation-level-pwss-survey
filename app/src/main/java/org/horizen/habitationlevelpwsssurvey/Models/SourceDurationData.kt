package org.horizen.habitationlevelpwsssurvey.Models

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import java.io.Serializable

@Entity
data class SourceDurationData(
        @PrimaryKey
        var id: String,
        var tube_id: String,
        var start_time: String,
        var end_time : String,
        var total_Duration: String
): Serializable {
    @Ignore
    constructor(): this("", "", "", "", "")
}