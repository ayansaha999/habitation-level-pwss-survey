package org.horizen.habitationlevelpwsssurvey.Models

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import java.io.Serializable

@Entity
data class KmlData(

        @PrimaryKey
        var id: String,
        var scheme_code: String,
        var scheme_name: String,
        var scheme_smcode: String,
        var distribution_main: String,
        var rising_main: String,
        var road: String,
        var pwss_boundary: String,
        var mouza_boundary: String,
        var damage_pipeline: String,
        var settlement: String,
        var center_lat: String,
        var center_lon: String
): Serializable {
    @Ignore
    constructor() : this("", "", "", "", "", "",
            "", "", "","", "", "", "")
}