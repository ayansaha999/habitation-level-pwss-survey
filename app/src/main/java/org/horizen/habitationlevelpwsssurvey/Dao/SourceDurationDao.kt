package org.horizen.habitationlevelpwsssurvey.Dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import org.horizen.habitationlevelpwsssurvey.Models.SourceDurationData

@Dao
interface SourceDurationDao {
    @Query("select * from SourceDurationData")
    fun getAllSourceDurationDatas(): LiveData<List<SourceDurationData>>

    @Query("select * from SourceDurationData where tube_id = :tubeId")
    fun getSourceDurationDataByTueId(tubeId: String): LiveData<SourceDurationData>

    @Query("select * from SourceDurationData where id = :id")
    fun getSourceDurationDataById(id: String): LiveData<SourceDurationData>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertSourceDurationData(vararg SourceDurationData: SourceDurationData)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateSourceDurationData(vararg SourceDurationData: SourceDurationData)

    @Delete
    fun deleteSourceDurationData(SourceDurationData: SourceDurationData)
}