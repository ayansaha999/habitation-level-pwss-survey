package org.horizen.habitationlevelpwsssurvey.Dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import org.horizen.habitationlevelpwsssurvey.Models.User

@Dao
interface UserDao {
    @Query("select * from User")
    fun getAllUsers(): LiveData<List<User>>

    @Query("select * from User")
    fun getAllUsersNormal(): List<User>

    @Query("select * from User where user_id = :userId")
    fun getUserByUserId(userId: String): LiveData<User>

    @Query("select * from User where status = :status")
    fun getUserByStatus(status: Boolean): LiveData<User>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUser(vararg User: User)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateUser(vararg User: User)

    @Delete
    fun deleteUser(User: User)
}