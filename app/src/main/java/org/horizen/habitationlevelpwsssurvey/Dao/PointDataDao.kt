package org.horizen.habitationlevelpwsssurvey.Dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import org.horizen.habitationlevelpwsssurvey.Models.PointData

@Dao
interface PointDataDao {
    
    @Query("select * from PointData")
    fun getAllPointDatas(): LiveData<List<PointData>>

    @Query("select * from PointData")
    fun getAllPointDatasNormal(): List<PointData>

    @Query("select * from PointData where scheme_code = :schmCode")
    fun getPointDataBySchemeCode(schmCode: String): LiveData<PointData>


    @Query("select * from PointData where uploaded = :upload")
    fun getPointDataByUploadStatus(upload: Boolean): List<PointData>
    
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPointData(vararg PointData: PointData)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updatePointData(vararg PointData: PointData)

    @Delete
    fun deletePointData(PointData: PointData)

}