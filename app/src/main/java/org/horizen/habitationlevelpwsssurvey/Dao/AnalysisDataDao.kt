package org.horizen.habitationlevelpwsssurvey.Dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import org.horizen.habitationlevelpwsssurvey.Models.AnalysisData

@Dao
interface AnalysisDataDao {
    
    @Query("select * from AnalysisData")
    fun getAllAnalysisDatas(): LiveData<List<AnalysisData>>

    @Query("select * from AnalysisData")
    fun getAllAnalysisDatasNormal(): List<AnalysisData>

    @Query("select * from AnalysisData where scheme_smcode = :schmCode")
    fun getAnalysisDataBySchemeCode(schmCode: String): List<AnalysisData>
    
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAnalysisData(vararg AnalysisData: AnalysisData)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateAnalysisData(vararg AnalysisData: AnalysisData)

    @Delete
    fun deleteAnalysisData(AnalysisData: AnalysisData)

}