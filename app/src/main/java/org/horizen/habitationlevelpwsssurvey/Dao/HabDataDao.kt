package org.horizen.habitationlevelpwsssurvey.Dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import org.horizen.habitationlevelpwsssurvey.Models.HabData

@Dao
interface HabDataDao {
    @Query("select * from HabData order by hab_name asc")
    fun getAllHabDatas(): LiveData<List<HabData>>

    @Query("select * from HabData where scheme_smcode = :smCode order by hab_name asc")
    fun getHabDataBySmCode(smCode: String): LiveData<HabData>

    @Query("select * from HabData where uploaded = :upload order by hab_name asc")
    fun getHabDataByUploadStatus(upload: Boolean): List<HabData>

    @Query("select * from HabData where id = :id")
    fun getHabDataById(id: String): LiveData<HabData>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertHabData(vararg HabData: HabData)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateHabData(vararg HabData: HabData)

    @Delete
    fun deleteHabData(HabData: HabData)
}