package org.horizen.habitationlevelpwsssurvey.Dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import org.horizen.habitationlevelpwsssurvey.Models.HwData

@Dao
interface HwDataDao {
    @Query("select * from HwData")
    fun getAllHwDatas(): LiveData<List<HwData>>

    @Query("select * from HwData where hab_name = :hab")
    fun getHwDataByHabitation(hab: String): LiveData<HwData>

    @Query("select * from HwData where id = :id")
    fun getHwDataById(id: String): LiveData<HwData>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertHwData(vararg HwData: HwData)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateHwData(vararg HwData: HwData)

    @Delete
    fun deleteHwData(HwData: HwData)
}