package org.horizen.habitationlevelpwsssurvey.Dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import org.horizen.habitationlevelpwsssurvey.Models.KmlData

@Dao
interface KmlDataDao {
    
    @Query("select * from KmlData")
    fun getAllKmlDatas(): LiveData<List<KmlData>>

    @Query("select * from KmlData")
    fun getAllKmlDatasNormal(): List<KmlData>

    @Query("select * from KmlData where scheme_smcode = :schmCode")
    fun getKmlDataBySchemeCode(schmCode: String): LiveData<KmlData>
    
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertKmlData(vararg KmlData: KmlData)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateKmlData(vararg KmlData: KmlData)

    @Delete
    fun deleteKmlData(KmlData: KmlData)

}