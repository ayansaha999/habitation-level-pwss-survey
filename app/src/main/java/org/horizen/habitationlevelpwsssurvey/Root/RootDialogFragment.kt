package org.horizen.habitationlevelpwsssurvey.Root

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v4.app.Fragment
import com.snatik.storage.Storage
import org.horizen.habitationlevelpwsssurvey.Helper.LocalDatabase
import org.horizen.habitationlevelpwsssurvey.Helper.TinyDB
import java.io.File

open class RootDialogFragment: DialogFragment() {

    lateinit var prefs: TinyDB
    lateinit var rootVm: RootVm
    lateinit var localDb: LocalDatabase
    protected lateinit var storage: Storage
    protected lateinit var path: String
    protected lateinit var mainPath: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        prefs = TinyDB(activity!!.applicationContext)
        rootVm = ViewModelProviders.of(this).get(RootVm::class.java)
        localDb = RootApplication.database!!
        storage = Storage(activity!!.applicationContext)
        path = storage.externalStorageDirectory
        mainPath = path + File.separator + "Habitation Level Pwss Survey" + File.separator + "Data"
        val dirExists = storage.isDirectoryExists(mainPath)
        if (!dirExists) {
            storage.createDirectory(mainPath)
        }
    }


}