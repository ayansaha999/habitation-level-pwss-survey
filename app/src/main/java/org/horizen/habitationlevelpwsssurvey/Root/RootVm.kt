package org.horizen.habitationlevelpwsssurvey.Root

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.location.Location
import android.view.View
import org.horizen.habitationlevelpwsssurvey.Models.*
import java.text.SimpleDateFormat
import java.util.*

class RootVm: ViewModel() {

    var passwordVisible = MutableLiveData<Boolean>()
    var authstatus = MutableLiveData<Int>()
    var actionMessage = MutableLiveData<String>()
    var activeUser = MutableLiveData<User>()

    var activeSection = MutableLiveData<Section>()

    var userDataList: LiveData<List<User>>? = MutableLiveData<List<User>>()
    var hwDataList: LiveData<List<HwData>>? = MutableLiveData<List<HwData>>()
    var habDataList: LiveData<List<HabData>>? = MutableLiveData<List<HabData>>()
    var sourceDurationList: LiveData<List<SourceDurationData>>? = MutableLiveData<List<SourceDurationData>>()
    var analysisPointList: LiveData<List<AnalysisData>>? = null
    var pointDataList: LiveData<List<PointData>>? = MutableLiveData<List<PointData>>()

    var currentLocation = MutableLiveData<Location>()
    var habFilterOpen = MutableLiveData<Boolean>()

    init {
        authstatus.value = 0
        actionMessage.value = ""
        activeSection.value = Section()
        activeUser.value = User()
        habFilterOpen.value = true
        passwordVisible.value = false
        userDataList = RootApplication.database?.userDao()?.getAllUsers()
        hwDataList = RootApplication.database?.hwDataDao()?.getAllHwDatas()
        habDataList = RootApplication.database?.habDataDao()?.getAllHabDatas()
        pointDataList = RootApplication.database?.pointDataDao()?.getAllPointDatas()
        sourceDurationList = RootApplication.database?.sourceDurationDataDao()?.getAllSourceDurationDatas()
        analysisPointList = RootApplication.database?.analysisDataDao()?.getAllAnalysisDatas()
    }

    fun getTodayDate(): String {
        val c = Calendar.getInstance()
        val df = SimpleDateFormat("yyyy-MM-dd")
        return df.format(c.time)
    }

    fun getTodayTime(): String {
        val c = Calendar.getInstance()
        val df = SimpleDateFormat("HH:mm:ss")
        return df.format(c.time)
    }
}