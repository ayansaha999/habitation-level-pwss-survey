package org.horizen.habitationlevelpwsssurvey.Root

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import org.horizen.habitationlevelpwsssurvey.Helper.LocalDatabase
import org.horizen.habitationlevelpwsssurvey.Helper.TinyDB

open class RootFragment: Fragment() {

    lateinit var prefs: TinyDB
    lateinit var rootVm: RootVm
    lateinit var localDb: LocalDatabase

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        prefs = TinyDB(activity!!.applicationContext)
        rootVm = ViewModelProviders.of(this).get(RootVm::class.java)
        localDb = RootApplication.database!!
    }


}