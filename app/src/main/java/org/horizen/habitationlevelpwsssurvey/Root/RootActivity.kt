package org.horizen.habitationlevelpwsssurvey.Root

import android.Manifest
import android.annotation.SuppressLint
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.location.Location
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.util.Log.e
import android.view.WindowManager
import android.widget.Toast
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.tasks.Task
import com.snatik.storage.Storage
import kotlinx.android.synthetic.main.activity_home.*
import okhttp3.*
import org.horizen.habitationlevelpwsssurvey.Helper.*
import org.horizen.habitationlevelpwsssurvey.Main.Home.HomeActivity
import org.horizen.habitationlevelpwsssurvey.Models.AnalysisData
import org.horizen.habitationlevelpwsssurvey.Models.HabData
import org.horizen.habitationlevelpwsssurvey.Models.User
import org.horizen.habitationlevelpwsssurvey.Root.RootVm
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.toast
import org.json.JSONArray
import org.json.JSONObject
import java.io.File
import java.lang.reflect.Type
import java.net.URL
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import android.arch.lifecycle.Observer
import android.os.StrictMode

@SuppressLint("Registered")
open class RootActivity : AppCompatActivity() {
    protected lateinit var fusedLocationClient: FusedLocationProviderClient
    protected lateinit var locationCallback: LocationCallback
    protected val REQUEST_PERMISSION = 0
    protected lateinit var mMap: GoogleMap
    protected lateinit var storage: Storage
    protected lateinit var path: String
    protected lateinit var mainPath: String
    lateinit var prefs: TinyDB
    lateinit var rootVm: RootVm
    lateinit var localDb: LocalDatabase

    protected var analysisPointList = arrayListOf<AnalysisData>()

    protected var analysisMarkerList = arrayListOf<MarkerOptions>()

    protected val serverRef = "http://maps.wbphed.gov.in/pwss_survey/mobile_service/"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        prefs = TinyDB(applicationContext)
        rootVm = ViewModelProviders.of(this).get(RootVm::class.java)
        localDb = RootApplication.database!!
        storage = Storage(applicationContext)
        path = storage.externalStorageDirectory
        mainPath = path + File.separator + "Habitation Level Pwss Survey" + File.separator + "Data"
        val dirExists = storage.isDirectoryExists(mainPath)
        if (!dirExists) {
            storage.createDirectory(mainPath)
        }
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                locationResult ?: return
                for (location in locationResult.locations) {
                    rootVm.currentLocation.value = location
                }
            }
        }

        rootVm.analysisPointList?.observe(this, Observer { analysisDataList ->
            if (analysisDataList != null) {
                val filteredAnalysisPointList = arrayListOf<AnalysisData>()
                for (analysisPoint in analysisDataList) {
                    analysisPointList.add(analysisPoint)
                    if (!analysisPoint.uploaded) {
                        filteredAnalysisPointList.add(analysisPoint)
                    }
                }

                uploadAnalysisData(filteredAnalysisPointList)
            }
        })
    }

    fun login(username: String, password: String) {
        doAsync {
            try {
                val valuePairs = ArrayList<ValuePair>()
                valuePairs.add(ValuePair("userid", username))

                valuePairs.add(ValuePair("password", password))

                e("Login Request", getRequestString("login.php", valuePairs))

                val resp = Server.callApi(serverRef + "login.php", valuePairs)
                e("Login Response", resp)

                val jobj = JSONObject(resp)
                if (!jobj.getBoolean("status")) {
                    runOnUiThread {
                        AlertDialog.Builder(this@RootActivity).setMessage("Wrong Username or Password.").setTitle("Login Failed").setPositiveButton("Close", null).create().show()
                    }
                } else {
                    if (jobj.getString("version") == "1.1.1") {
                        runOnUiThread {
                            rootVm.authstatus.value = 2
                        }

                        val user = DataTypeConverter().fromStringToUser(resp)

                        runOnUiThread {
                            rootVm.activeUser.value = user
                        }

                        localDb.userDao().insertUser(user!!)

                    } else {
                        runOnUiThread {
                            rootVm.authstatus.value = 0
                        }
                    }
                }
            } catch (ex: Exception) {
                ex.printStackTrace()
                toast("Login Error" + ex.message)
                runOnUiThread {
                    rootVm.authstatus.value = 0
                }
            }
        }
    }

    fun downloadHwData(user: User) {
        rootVm.actionMessage.value = "Downloading head work site data"
        doAsync {
            try {
                val valuePairs = ArrayList<ValuePair>()
                valuePairs.add(ValuePair("dist", user.dist_id))
                valuePairs.add(ValuePair("block", user.block_id))
                valuePairs.add(ValuePair("session_key", user.session_key))
                e("HW Data Request", getRequestString("get-hw-offline-data.php", valuePairs))

                val resp = Server.callApi(serverRef + "get-hw-offline-data.php", valuePairs)
                e("HW Data Response", resp)

                val jArray = JSONArray(resp)
                for (index in 0 until jArray.length()) {
                    val o = jArray.getJSONObject(index)
                    val hwData = DataTypeConverter().fromStringToHwData(o.toString())
                    localDb.hwDataDao().insertHwData(hwData!!)
                }

                downloadHabData(user)
            } catch (ex: Exception) {
                ex.printStackTrace()
                e("HW Data Error", ex.toString())
            }
        }
    }

    private fun downloadHabData(user: User) {
        runOnUiThread {
            rootVm.actionMessage.value = "Downloading habitation data"
        }
        try {
            val valuePairs = ArrayList<ValuePair>()
            valuePairs.add(ValuePair("dist", user.dist_id))
            valuePairs.add(ValuePair("block", user.block_id))
            valuePairs.add(ValuePair("session_key", user.session_key))
            e("HW Data Request", getRequestString("get-hw-offline-habdata.php", valuePairs))

            val resp = Server.callApi(serverRef + "get-hw-offline-habdata.php", valuePairs)
            e("HW Data Response", resp)

            val jArray = JSONArray(resp)
            for (index in 0 until jArray.length()) {
                val o = jArray.getJSONObject(index)
                val hwData = DataTypeConverter().fromStringToHabData(o.toString())
                hwData!!.user_id = user.user_id
                hwData.block_id = user.block_id
                hwData.dist_id = user.dist_id
                hwData.supply_level = "0"
                localDb.habDataDao().insertHabData(hwData)
            }

            downloadSupplyDuration(user)
        } catch (ex: Exception) {
            ex.printStackTrace()
            e("HW Data Error", ex.message)
        }
    }

    private fun downloadSupplyDuration(user: User) {
        runOnUiThread {
            rootVm.actionMessage.value = "Downloading source supply duration data"
        }
        try {
            val valuePairs = ArrayList<ValuePair>()
            valuePairs.add(ValuePair("dist", user.dist_id))
            valuePairs.add(ValuePair("block", user.block_id))
            valuePairs.add(ValuePair("session_key", user.session_key))
            e("HW Data Request", getRequestString("get-hw-source-duration.php", valuePairs))

            val resp = Server.callApi(serverRef + "get-hw-source-duration.php", valuePairs)
            e("HW Data Response", resp)

            val jArray = JSONArray(resp)
            for (index in 0 until jArray.length()) {
                val o = jArray.getJSONObject(index)
                val hwData = DataTypeConverter().fromStringToSourceDurationData(o.toString())
                localDb.sourceDurationDataDao().insertSourceDurationData(hwData!!)
            }

            downloadKmlData(user)
        } catch (ex: Exception) {
            ex.printStackTrace()
            e("HW Data Error", ex.message)
        }
    }

    private fun downloadKmlData(user: User) {
        runOnUiThread {
            rootVm.actionMessage.value = "Downloading kml data"
        }
        try {
            val valuePairs = ArrayList<ValuePair>()
            valuePairs.add(ValuePair("dist", user.dist_id))
            valuePairs.add(ValuePair("block", user.block_id))
            valuePairs.add(ValuePair("session_key", user.session_key))
            e("HW Data Request", getRequestString("get-kml-files.php", valuePairs))

            val resp = Server.callApi(serverRef + "get-kml-files.php", valuePairs)
            e("HW Data Response", resp)

            val jArray = JSONArray(resp)
            for (index in 0 until jArray.length()) {
                val o = jArray.getJSONObject(index)
                val hwData = DataTypeConverter().fromStringToKmlData(o.toString())
                localDb.kmlDataDao().insertKmlData(hwData!!)
            }

            downloadSourcePointData(user)
        } catch (ex: Exception) {
            ex.printStackTrace()
            e("HW Data Error", ex.message)
        }
    }

    private fun downloadSourcePointData(user: User) {
        runOnUiThread {
            rootVm.actionMessage.value = "Downloading source data"
        }
        try {
            val valuePairs = ArrayList<ValuePair>()
            valuePairs.add(ValuePair("dist", user.dist_id))
            valuePairs.add(ValuePair("block", user.block_id))
            valuePairs.add(ValuePair("session_key", user.session_key))
            e("HW Data Request", getRequestString("get-hw-offline-standpost.php", valuePairs))

            val resp = Server.callApi(serverRef + "get-hw-offline-standpost.php", valuePairs)
            e("HW Data Response", resp)

            val jArray = JSONArray(resp)
            for (index in 0 until jArray.length()) {
                val o = jArray.getJSONObject(index)
                val hwData = DataTypeConverter().fromStringToPointData(o.toString())
                localDb.pointDataDao().insertPointData(hwData!!)
            }

            downloadAnalysisPointData(user)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun downloadAnalysisPointData(user: User) {
        runOnUiThread {
            rootVm.actionMessage.value = "Downloading analysis data"
        }
        try {
            val valuePairs = ArrayList<ValuePair>()
            valuePairs.add(ValuePair("block", user.block_id))
            valuePairs.add(ValuePair("session_key", user.session_key))
            e("Analysis Data Request", getRequestString("get-sp-offline-data.php", valuePairs))

            val resp = Server.callApi(serverRef + "get-sp-offline-data.php", valuePairs)
            e("Analysis Response", resp)

            val jArray = JSONArray(resp)
            for (index in 0 until jArray.length()) {
                val o = jArray.getJSONObject(index)
                val hwData = DataTypeConverter().fromStringToAnalysisData(o.toString())
                localDb.analysisDataDao().insertAnalysisData(hwData!!)
                e("Point Check", hwData.question)
            }


            getRoadsTask()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun getRoadsTask() {
        runOnUiThread {
            rootVm.actionMessage.value = "Downloading road data"
        }
        val dataList = localDb.kmlDataDao().getAllKmlDatasNormal()
        for (i in 0 until dataList.size) {
            val data = dataList[i]
            val fileName = "road.kml"
            try {
                val inputStream = URL(data.road).openStream()
                val settlePath = mainPath + "/" + data.scheme_smcode
                Log.e("local_road_path", settlePath)
                val dirExists = storage.isDirectoryExists(settlePath)
                if (!dirExists) {
                    storage.createDirectory(settlePath)
                }
                inputStream.use { input ->
                    File("$settlePath/$fileName").outputStream().use { fileOut ->
                        input.copyTo(fileOut)
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
                Log.e("Eror", e.message)

            }
        }
        getSchemeBoundaryTask()
    }

    private fun getSchemeBoundaryTask() {
        runOnUiThread {
            rootVm.actionMessage.value = "Downloading scheme boundary data"
        }
        val dataList = localDb.kmlDataDao().getAllKmlDatasNormal()
        for (i in 0 until dataList.size) {
            val data = dataList[i]
            val fileName = "scheme_boundary.kml"
            try {
                val inputStream = URL(data.pwss_boundary).openStream()
                val settlePath = mainPath + "/" + data.scheme_smcode
                Log.e("local_scheme_path", settlePath)
                val dirExists = storage.isDirectoryExists(settlePath)
                if (!dirExists) {
                    storage.createDirectory(settlePath)
                }
                inputStream.use { input ->
                    File("$settlePath/$fileName").outputStream().use { fileOut ->
                        input.copyTo(fileOut)
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
                Log.e("Eror", e.message)
            }
        }
        getMouzaBoundaryTask()
    }

    private fun getMouzaBoundaryTask() {
        runOnUiThread {
            rootVm.actionMessage.value = "Downloading mouza boundary data"
        }
        val dataList = localDb.kmlDataDao().getAllKmlDatasNormal()
        for (i in 0 until dataList.size) {
            val data = dataList[i]
            val fileName = "mouza.kml"
            try {
                val inputStream = URL(data.mouza_boundary).openStream()
                val settlePath = mainPath + "/" + data.scheme_smcode
                Log.e("local_mouza_path", settlePath)
                val dirExists = storage.isDirectoryExists(settlePath)
                if (!dirExists) {
                    storage.createDirectory(settlePath)
                }
                inputStream.use { input ->
                    File("$settlePath/$fileName").outputStream().use { fileOut ->
                        input.copyTo(fileOut)
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
                Log.e("Eror", e.message)
            }
        }
        getHabitationTask()
    }

    private fun getHabitationTask() {
        runOnUiThread {
            rootVm.actionMessage.value = "Downloading habitation boundary data"
        }
        val dataList = localDb.kmlDataDao().getAllKmlDatasNormal()
        for (i in 0 until dataList.size) {
            val data = dataList[i]
            val fileName = "habitation.kml"
            try {
                val inputStream = URL(data.settlement).openStream()
                val settlePath = mainPath + "/" + data.scheme_smcode
                Log.e("local_habitation_path", settlePath)
                val dirExists = storage.isDirectoryExists(settlePath)
                if (!dirExists) {
                    storage.createDirectory(settlePath)
                }
                inputStream.use { input ->
                    File("$settlePath/$fileName").outputStream().use { fileOut ->
                        input.copyTo(fileOut)
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
                Log.e("Eror", e.message)

            }
        }
        getDistributionMainTask()
    }

    private fun getDistributionMainTask() {
        runOnUiThread {
            rootVm.actionMessage.value = "Downloading distribution main data"
        }
        val dataList = localDb.kmlDataDao().getAllKmlDatasNormal()
        for (i in 0 until dataList.size) {
            val data = dataList[i]
            val fileName = "distribution_main.kml"
            try {
                val inputStream = URL(data.distribution_main).openStream()
                val settlePath = mainPath + "/" + data.scheme_smcode
                Log.e("local_dist_path", settlePath)
                val dirExists = storage.isDirectoryExists(settlePath)
                if (!dirExists) {
                    storage.createDirectory(settlePath)
                }
                inputStream.use { input ->
                    File("$settlePath/$fileName").outputStream().use { fileOut ->
                        input.copyTo(fileOut)
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
                Log.e("Eror", e.message)

            }
        }
        getDamagedDistributionMainTask()
    }

    private fun getDamagedDistributionMainTask() {
        runOnUiThread {
            rootVm.actionMessage.value = "Downloading distribution main data"
        }
        val dataList = localDb.kmlDataDao().getAllKmlDatasNormal()
        for (i in 0 until dataList.size) {
            val data = dataList[i]
            val fileName = "damaged_distribution_main.kml"
            try {
                val inputStream = URL(data.damage_pipeline).openStream()
                val settlePath = mainPath + "/" + data.scheme_smcode
                Log.e("local_damaged_dist_path", settlePath)
                val dirExists = storage.isDirectoryExists(settlePath)
                if (!dirExists) {
                    storage.createDirectory(settlePath)
                }
                inputStream.use { input ->
                    File("$settlePath/$fileName").outputStream().use { fileOut ->
                        input.copyTo(fileOut)
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
                Log.e("Eror", e.message)

            }
        }
        getRisingMainTask()
    }

    private fun getRisingMainTask() {
        runOnUiThread {
            rootVm.actionMessage.value = "Downloading rising main data"
        }
        val dataList = localDb.kmlDataDao().getAllKmlDatasNormal()
        for (i in 0 until dataList.size) {
            val data = dataList[i]
            val fileName = "rising_main.kml"
            try {
                val inputStream = URL(data.rising_main).openStream()
                val settlePath = mainPath + "/" + data.scheme_smcode
                Log.e("local_road_path", settlePath)
                val dirExists = storage.isDirectoryExists(settlePath)
                if (!dirExists) {
                    storage.createDirectory(settlePath)
                }
                inputStream.use { input ->
                    File("$settlePath/$fileName").outputStream().use { fileOut ->
                        input.copyTo(fileOut)
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
                Log.e("Eror", e.message)

            }
        }
        getImagesTask()
    }

    private fun getImagesTask() {
        runOnUiThread {
            rootVm.actionMessage.value = "Downloading image data"
        }

        val dataList = localDb.pointDataDao().getAllPointDatasNormal()
        for (i in 0 until dataList.size) {
            val data = dataList[i]
            if (!data.imagepath.isNullOrEmpty()) {
                runOnUiThread {
                    val percent = i / dataList.size * 100
                    rootVm.actionMessage.value = "Downloading image data $percent% ($i/${dataList.size})"
                }
                val fileName = data.imagepath!!.substring(data.imagepath!!.lastIndexOf("/") + 1)
                try {
                    val inputStream = URL("http://maps.wbphed.gov.in/spot_source/mobile_service_test/thumb/${data.imagepath}").openStream()
                    val settlePath = "$mainPath/Images"
                    Log.e("local_image_path", settlePath)
                    val dirExists = storage.isDirectoryExists(settlePath)
                    if (!dirExists) {
                        storage.createDirectory(settlePath)
                    }
                    inputStream.use { input ->
                        File("$settlePath/$fileName").outputStream().use { fileOut ->
                            input.copyTo(fileOut)
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                    Log.e("Eror", e.message)

                }
            }
        }
        runOnUiThread {
            prefs.putBoolean(Defaults.LOGGED_IN, true)
            startActivity(Intent(this@RootActivity, HomeActivity::class.java))
        }
    }

    fun getRequestString(api: String, valuePairs: ArrayList<ValuePair>): String {
        var request = "$serverRef$api?"
        for (valuePair in valuePairs) {
            request += "${valuePair.name}=${valuePair.value}&&"
        }
        return request
    }

    private val locationRequest = LocationRequest().apply {
        interval = 5000
        fastestInterval = 2000
        priority = LocationRequest.PRIORITY_HIGH_ACCURACY
    }

    fun startLocationService() {

        val builder = LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest)
        val client: SettingsClient = LocationServices.getSettingsClient(this)
        val task: Task<LocationSettingsResponse> = client.checkLocationSettings(builder.build())

        task.addOnSuccessListener { _ ->
            getLastLocation()
        }

        task.addOnFailureListener { exception ->
            if (exception is ResolvableApiException) {
                try {
                    exception.startResolutionForResult(this@RootActivity, 2)
                } catch (sendEx: IntentSender.SendIntentException) {

                }
            }
        }
    }

    @SuppressLint("MissingPermission")
    fun getLastLocation() {
        fusedLocationClient.lastLocation.addOnSuccessListener { location: Location? ->
            if (location != null) {
                val cameraUpdate = CameraUpdateFactory.newCameraPosition(CameraPosition.fromLatLngZoom(LatLng(location.latitude, location.longitude), 20f))
                mMap.animateCamera(cameraUpdate)
            }
        }
    }

    @SuppressLint("MissingPermission")
    fun startLocationUpdates() {
        if (checkPermissions()) {
            fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null /* Looper */)
        }
    }

    fun stopLocationUpdates() {
        fusedLocationClient.removeLocationUpdates(locationCallback)
    }

    fun checkPermissions(): Boolean {
        return if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            true
        } else {
            requestPermissions()
            false
        }
    }

    private fun requestPermissions() {
        ActivityCompat.requestPermissions(this,
                arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO),
                REQUEST_PERMISSION)
    }

    fun uploadHabData() {
        if (isNetworkAvailable()) {
            doAsync {
                val habData = RootApplication.database?.habDataDao()?.getHabDataByUploadStatus(false)
                val userData = RootApplication.database?.userDao()?.getAllUsersNormal()
                var sessionKey = ""
                for (user in userData!!) {
                    if (user.status) {
                        sessionKey = user.session_key
                    }
                }
                if(habData != null && habData.isNotEmpty()) {
                    val habDataString = DataTypeConverter().fromHabDataListToString(habData)
                    Log.e("UpdatedHab", habDataString)
                    try {
                        val valuePairs = ArrayList<ValuePair>()
                        valuePairs.add(ValuePair("upload_data", habDataString!!))
                        valuePairs.add(ValuePair("session_key", sessionKey))
                        Log.e("HW Data Request", getRequestString("upload_coverage_data.php", valuePairs))

                        val resp = Server.callApi(serverRef + "upload_coverage_data.php", valuePairs)
                        Log.e("HW Data Response", resp)

                        val jArray = JSONArray(habDataString)
                        for (index in 0 until jArray.length()) {
                            val o = jArray.getJSONObject(index)
                            val hwData = DataTypeConverter().fromStringToHabData(o.toString())
                            hwData!!.uploaded = true
                            localDb.habDataDao().updateHabData(hwData)
                        }
                    } catch (ex: Exception) {
                        ex.printStackTrace()
                        Log.e("HW Data Error", ex.message)
                    }
                }
            }
        } else {
            toast("Data not synced due to lack of network connection!")
        }
    }

    fun uploadSupplyPointData() {
        if (isNetworkAvailable()) {
            doAsync {
                val pointData = RootApplication.database?.pointDataDao()?.getPointDataByUploadStatus(false)
                val userData = RootApplication.database?.userDao()?.getAllUsersNormal()
                var sessionKey = ""
                for (user in userData!!) {
                    if (user.status) {
                        sessionKey = user.session_key
                    }
                }

                if(pointData != null && pointData.isNotEmpty()) {

                    val habDataString = DataTypeConverter().fromPointDataListToString(pointData)
                    Log.e("Updated FLow Data", habDataString)
                    try {
                        val valuePairs = ArrayList<ValuePair>()
                        valuePairs.add(ValuePair("upload_data", habDataString!!))
                        valuePairs.add(ValuePair("session_key", sessionKey))
                        Log.e("Upload Flow Request", getRequestString("upload_water_level_data.php", valuePairs))

                        val resp = Server.callApi(serverRef + "upload_water_level_data.php", valuePairs)
                        Log.e("Upload Flow Response", resp)

                        val jArray = JSONArray(habDataString)
                        for (index in 0 until jArray.length()) {
                            val o = jArray.getJSONObject(index)
                            val hwData = DataTypeConverter().fromStringToHabData(o.toString())
                            hwData!!.uploaded = true
                            localDb.habDataDao().updateHabData(hwData)
                        }
                    } catch (ex: Exception) {
                        ex.printStackTrace()
                        Log.e("HW Data Error", ex.message)
                    }
                }
            }
        } else {
            toast("Data not synced due to lack of network connection!")
        }
    }

    private fun uploadAnalysisData(imageList: ArrayList<AnalysisData>) {
        for (image in imageList) {
            val habDataString = DataTypeConverter().fromObjectToJsonString(image)
            e("Upload Analysis Data", habDataString)
            try {
                val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
                StrictMode.setThreadPolicy(policy)
                val path = image.image_path
                val filename = path.substring(path.lastIndexOf("/") + 1)
                val client = OkHttpClient.Builder().connectTimeout(30, TimeUnit.SECONDS).readTimeout(30, TimeUnit.MINUTES).writeTimeout(30, TimeUnit.MINUTES).build()
                val requestBody = MultipartBody.Builder()
                        .setType(MultipartBody.FORM)
                        .addFormDataPart("sp_id", image.sp_id)
                        .addFormDataPart("user_id", image.user_id)
                        .addFormDataPart("number", image.number)
                        .addFormDataPart("rermarks", image.remarks)
                        .addFormDataPart("device_date", image.device_date)
                        .addFormDataPart("device_time", image.device_time)
                        .addFormDataPart("session_key", "81dc9bdb52d04dc20036dbd8313ed055")
                        .addFormDataPart("file", filename,
                                RequestBody.create(MediaType.parse("image/jpg"), File(path)))

                val request = Request.Builder()
                        .url(serverRef + "sp_data_file_upload.php")
                        .post(requestBody.build())
                        .build()

                val resp = client.newCall(request).execute()
                val s = resp.body()!!.string()

                Log.e("resp", s)
                val array = JSONArray(s)
                for (index in 0 until array.length()) {
                    val obj = array.getJSONObject(index)
                    val status = obj.getString("status")
                    if (status.contains("success")) {
                        image.uploaded = true
                        doAsync {
                            localDb.analysisDataDao().updateAnalysisData(image)
                        }
                    } else {
                        e("Failed", "Image Upload Failed")
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
                e("Error", e.toString())
                if (e.message != null) {
                    Toast.makeText(this, e.message, Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE)
        return if (connectivityManager is ConnectivityManager) {
            val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo
            networkInfo?.isConnected ?: false
        } else false
    }
}

