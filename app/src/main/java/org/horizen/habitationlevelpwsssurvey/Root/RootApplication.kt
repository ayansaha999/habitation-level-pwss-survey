package org.horizen.habitationlevelpwsssurvey.Root

import android.app.Application
import android.arch.persistence.room.Room
import android.support.multidex.MultiDex
import org.horizen.habitationlevelpwsssurvey.Helper.LocalDatabase

class RootApplication: Application() {
    companion object {
        var database: LocalDatabase? = null
    }

    override fun onCreate() {
        super.onCreate()
        MultiDex.install(this)
        RootApplication.database = Room.databaseBuilder(this, LocalDatabase::class.java, "habitation-level-pwss-survey-db").fallbackToDestructiveMigration().build()
    }
}