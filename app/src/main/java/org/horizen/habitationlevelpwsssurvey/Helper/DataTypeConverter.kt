package org.horizen.habitationlevelpwsssurvey.Helper

import android.arch.persistence.room.TypeConverter

import com.google.common.reflect.TypeToken
import com.google.gson.Gson
import org.horizen.habitationlevelpwsssurvey.Models.*
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class DataTypeConverter {

    @TypeConverter
    fun fromStringToUser(data: String): User? {
        val dataType = object : TypeToken<User>() {}.type
        return Gson().fromJson<User>(data, dataType)
    }

    @TypeConverter
    fun fromStringToHwData(data: String): HwData? {
        val dataType = object : TypeToken<HwData>() {}.type
        return Gson().fromJson<HwData>(data, dataType)
    }

    @TypeConverter
    fun fromStringToHabData(data: String): HabData? {
        val dataType = object : TypeToken<HabData>() {}.type
        return Gson().fromJson<HabData>(data, dataType)
    }

    @TypeConverter
    fun fromHabDataListToString(data: List<HabData>): String? {
        val gson = Gson()
        return gson.toJson(data)
    }

    @TypeConverter
    fun fromStringToKmlData(data: String): KmlData? {
        val dataType = object : TypeToken<KmlData>() {}.type
        return Gson().fromJson<KmlData>(data, dataType)
    }

    @TypeConverter
    fun fromStringToPointData(data: String): PointData? {
        val dataType = object : TypeToken<PointData>() {}.type
        return Gson().fromJson<PointData>(data, dataType)
    }

    @TypeConverter
    fun fromPointDataListToString(data: List<PointData>): String? {
        val gson = Gson()
        return gson.toJson(data)
    }

    @TypeConverter
    fun fromStringToAnalysisData(data: String): AnalysisData? {
        val dataType = object : TypeToken<AnalysisData>() {}.type
        return Gson().fromJson<AnalysisData>(data, dataType)
    }

    @TypeConverter
    fun fromStringToSourceDurationData(data: String): SourceDurationData? {
        val dataType = object : TypeToken<SourceDurationData>() {}.type
        return Gson().fromJson<SourceDurationData>(data, dataType)
    }

    @TypeConverter
    fun fromStringToDate(data: String): Date? {
        val dataType = object : TypeToken<Date>() {}.type
        return Gson().fromJson<Date>(data, dataType)
    }

    @TypeConverter
    fun fromDateToString(date: Date): String {
        val gson = Gson()
        return gson.toJson(date)
    }

    fun fromObjectToJsonString(data: Any): String {
        val gson = Gson()
        return gson.toJson(data)
    }

    fun fromObjectToArrayList(data: Any): ArrayList<ValuePair> {
        val dataString = fromObjectToJsonString(data)
        return fromStringToArrayList(dataString)
    }

    fun fromStringToArrayList(dataString: String): ArrayList<ValuePair> {
        val result : ArrayList<ValuePair> = ArrayList()
        val json = JSONObject(dataString)
        val keys = json.keys()
        while (keys.hasNext()) {
            val key = keys.next()
            try {
                val value = json.getString(key)
                result.add(ValuePair(key, value))
            } catch (e: JSONException) {
                e.printStackTrace()
            }

        }
        return result
    }
}