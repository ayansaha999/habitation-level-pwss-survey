package org.horizen.habitationlevelpwsssurvey.Helper

object Defaults {
    const val LOGGED_IN = "loggedIn"
    const val STAND_POST_WORKING = "STAND_POST_WORKING"
    const val STAND_POST_NOT_WORKING = "STAND_POST_NOT_WORKING"
    const val DISTRIBUTION_MAIN_UNDAMAGED = "DISTRIBUTION_MAIN_UNDAMAGED"
    const val DISTRIBUTION_MAIN_DAMAGED = "DISTRIBUTION_MAIN_DAMAGED"
    const val RISING_MAIN = "RISING_MAIN"
    const val MOUZA = "MOUZA"
    const val HABITATION = "HABITATION"
    const val PWSS = "PWSS"
    const val ROAD = "ROAD"
    const val ANALYSIS_POINT_Q = "ANALYSIS_POINT_Q"
    const val ANALYSIS_POINT_S = "ANALYSIS_POINT_S"

}