package org.horizen.habitationlevelpwsssurvey.Helper

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import org.horizen.habitationlevelpwsssurvey.Dao.*
import org.horizen.habitationlevelpwsssurvey.Models.*

@Database(entities = [(User::class), (HwData::class), (HabData::class), (SourceDurationData::class),
    (KmlData::class), (PointData::class), (AnalysisData::class)], version = 1)
@TypeConverters(DataTypeConverter::class)
abstract class LocalDatabase: RoomDatabase() {

    abstract fun userDao(): UserDao

    abstract fun hwDataDao(): HwDataDao

    abstract fun habDataDao(): HabDataDao

    abstract fun sourceDurationDataDao(): SourceDurationDao

    abstract fun kmlDataDao(): KmlDataDao

    abstract fun pointDataDao(): PointDataDao

    abstract fun analysisDataDao(): AnalysisDataDao
}